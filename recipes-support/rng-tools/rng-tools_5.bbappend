SUMMARY = "Random number generator daemon"
DESCRIPTION = "replace default config file with our own, and run initscript sooner"

FILESEXTRAPATHS_prepend := "${THISDIR}/rng-tools:"

#start rng-tools before networking so if wlan0 is set to auto start, boot does
#not stall waiting for the random number pool to initialize

INITSCRIPT_NAME = "rng-tools"
INITSCRIPT_PARAMS = "start 01 2 3 4 5 . stop 99 0 6 1 ."
