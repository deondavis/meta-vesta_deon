FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

LIC_FILES_CHKSUM += "\
file://LICENSE.QualcommAtheros_ar3k;md5=b5fe244fb2b532311de1472a3bc06da5"

FWPATH = "/lib/firmware/ar3k"

do_install_append () {
	install -m 444 ar3k/1020201/PS_ASIC.pst ${D}${FWPATH}/1020201
	install -m 444 ar3k/1020201/RamPatch.txt ${D}${FWPATH}/1020201
}

FILES_${PN}-ar3k = "  \
  ${FWPATH}/1020201/PS_ASIC.pst  \
  ${FWPATH}/1020201/RamPatch.txt  \
"