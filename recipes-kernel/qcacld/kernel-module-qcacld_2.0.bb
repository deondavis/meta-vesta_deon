SUMMARY = "Qualcomm WiFi driver module for QCA9733, compatible with 4.1 kernel"
LICENSE = "BSD & GPLv2"
LIC_FILES_CHKSUM = "file://CORE/HDD/src/wlan_hdd_main.c;beginline=1;endline=20;md5=c191a07d6df8a17ee5b865137f729304;" 

inherit module

SRCREV = "cb598adce4c1136fbf61c5dc7754fb962d72da87"

SRC_URI = "git://github.com/boundarydevices/qcacld-2.0;protocol=https;branch=boundary-LNX.LEH.4.2.2.2; \
	file://0001-prepend-ath10k-to-firmware-filenames.patch \
"

S = "${WORKDIR}/git"

EXTRA_OEMAKE += "CONFIG_CLD_HL_SDIO_CORE=y"

# The inherit of module.bbclass will automatically name module packages with
# "kernel-module-" prefix as required by the oe-core build environment.
