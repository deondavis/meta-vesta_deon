require linux-fslc-imx.inc

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRCREV = "1c7f6c63f9d0f4c3b8ba69236beea46345af821f"

COMPATIBLE_MACHINE = "(mx6|mx7|vesta)"