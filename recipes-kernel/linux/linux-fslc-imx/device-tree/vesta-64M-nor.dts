/*
 * Copyright (C) 2017 Rigado
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

/dts-v1/;

#include "vesta.dtsi"

/ {
	model = "Rigado Vesta 64M Nor Board";
	compatible = "fsl,vesta-64M-nor", "fsl,imx6ul";
	version = "1.0.0";
};

&qspi {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_qspi>;
	status = "okay";
	ddrsmp=<0>;

	flash0: mx25l25635e@0 {
		#address-cells = <1>;
		#size-cells = <1>;
		compatible = "micron,mx25l25645g";
		spi-max-frequency = <29000000>;
		spi-nor,ddr-quad-read-dummy = <6>;
		reg = <0>;
		partition@0 {
			/* This location must not be altered  */
			/* 1MB uboot */
			reg = <0 0x100000>;
			label = "u-boot";
		};
		partition@1 {
			/* This location must not be altered  */
			/* 7MB for dtb and kernel Image */
			reg = <0x100000 0x700000>;
			label = "boot";
		};
		partition@2 {
			/* This location must not be altered  */
			/* 24MB rootfs */
			reg = <0x800000 0x1800000>;
			label = "rootfs";
		};
		partition@3 {
			/* This location must not be altered  */
			/* 1MB for edata */
			reg = <0x2000000 0x100000>;
			label = "e-data";
		};
		partition@4 {
			/* This location must not be altered  */
			/* 7MB for alternate dtb and kernel Image */
			reg = <0x2100000 0x700000>;
			label = "alt_boot";
		};
		partition@5 {
			/* This location must not be altered  */
			/* 24MB for alternate rootfs */
			reg = <0x2800000 0x1800000>;
			label = "rootfs";
		};
	};
};

&usdhc1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_usdhc1 &pinctrl_wifi>;
	no-1-8-v;
	non-removable;
	bus-width = <4>;
	pm-ignore-notify;
	status = "okay";
};

&usdhc2 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_usdhc2>;
	no-1-8-v;
	non-removable;
	bus-width = <4>;
	keep-power-in-suspend;
	enable-sdio-wakeup;
	status = "okay";
};
