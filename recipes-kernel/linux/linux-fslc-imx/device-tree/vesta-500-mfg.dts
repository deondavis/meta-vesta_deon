/*
 * Copyright (C) 2018 Rigado
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

/dts-v1/;

#include <dt-bindings/input/input.h>
#include "imx6ull.dtsi"

/ {
        model = "Rigado Vesta 500 Board ";
        compatible = "fsl,vesta-500", "fsl,imx6ull";
        version = "1.0.0";

	chosen {
		stdout-path = &uart2;
	};

	memory {
		reg = <0x80000000 0x8000000>;
	};

	reserved-memory {
		#address-cells = <1>;
		#size-cells = <1>;
		ranges;

		linux,cma {
			compatible = "shared-dma-pool";
			reusable;
			size = <0x200000>;
			linux,cma-default;
		};
	};

	regulators {
		compatible = "simple-bus";
		#address-cells = <1>;
		#size-cells = <0>;

		reg_can_3v3: regulator@0 {
			compatible = "regulator-fixed";
			reg = <0>;
			regulator-name = "can-3v3";
			regulator-min-microvolt = <3300000>;
			regulator-max-microvolt = <3300000>;
		};

		reg_vref_3v: regulator@1 {
			  compatible = "regulator-fixed";
			  regulator-name = "vref-3v";
			  regulator-min-microvolt = <3075000>;
			  regulator-max-microvolt = <3075000>;
	  	};
	};

};

&cpu0 {
	arm-supply = <&reg_arm>;
	soc-supply = <&reg_soc>;
};

&fec1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_enet1>;
	phy-mode = "rmii";
	phy-handle = <&ethphy1>;
	status = "okay";

	mdio {
		#address-cells = <1>;
		#size-cells = <0>;

		ethphy0: ethernet-phy@1 {
			compatible = "ethernet-phy-ieee802.3-c22";
			reg = <1>;
		};

		ethphy1: ethernet-phy@2 {
			compatible = "ethernet-phy-ieee802.3-c22";
			reg = <2>;
		};
	};
};

&gpc {
	fsl,cpu_pupscr_sw2iso = <0x1>;
	fsl,cpu_pupscr_sw = <0x0>;
	fsl,cpu_pdnscr_iso2sw = <0x1>;
	fsl,cpu_pdnscr_iso = <0x1>;
	fsl,ldo-bypass = <0>; /* DCDC, ldo-enable */
};

&i2c1 {
	clock-frequency = <100000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_i2c1>;
	status = "okay";
};

&i2c2 {
	clock_frequency = <100000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_i2c2>;
	status = "okay";

	exp_eeprom@50 {
		compatible = "24c32";
		reg = <0x50>;
		pagesize = <32>;
		#address-cells = <1>;
		#size-cells = <1>;
		exp0_data: exp_data@0 {
			reg = <0 0x4A>;
		};
	};
};

&adc1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_adc1>;
	status = "okay";
	vref-supply = <&reg_vref_3v>;
};

&iomuxc {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_hog_1>;
	imx6ul_evk: imx6ul_evk {
		pinctrl_hog_1: hoggrp-1 {
			fsl,pins = <
				MX6UL_PAD_GPIO1_IO02__GPIO1_IO02	0x80000000 /* GPIO Exp */
				MX6UL_PAD_GPIO1_IO08__GPIO1_IO08	0x80000000 /* SPDIF OUT */
				MX6UL_PAD_LCD_DATA00__GPIO3_IO05	0x00001020 /* BLE_RST_N */
				MX6UL_PAD_LCD_DATA08__GPIO3_IO13	0x00001020 /* SWDIO */
				MX6UL_PAD_LCD_DATA09__GPIO3_IO14	0x00001020 /* SWCLK */
				MX6UL_PAD_GPIO1_IO06__GPIO1_IO06	0x00001020 /* PMIC INT N */
				MX6UL_PAD_CSI_DATA02__GPIO4_IO23	0x00001020 /* BT GPIO */
				MX6UL_PAD_CSI_DATA03__GPIO4_IO24	0x00001020 /* BT RST N */
				MX6UL_PAD_CSI_DATA04__GPIO4_IO25	0x00001020 /* ENET1 RST N */
				MX6UL_PAD_CSI_DATA05__GPIO4_IO26	0x00001020 /* RST BTN  */
				MX6UL_PAD_CSI_DATA06__GPIO4_IO27	0x00001020 /* BT INT IN */
				MX6UL_PAD_CSI_DATA07__GPIO4_IO28	0x00001020 /* ENET1 INT N */
				MX6UL_PAD_NAND_ALE__USDHC2_RESET_B	0x000010A0 /* eMMC RST N */
				MX6UL_PAD_BOOT_MODE0__GPIO5_IO10	0x00013020 /* user btn */
			>;
		};

		pinctrl_adc1: adc1grp {
			fsl,pins = <
				MX6UL_PAD_GPIO1_IO00__GPIO1_IO00	0x00001020
				MX6UL_PAD_GPIO1_IO01__GPIO1_IO01	0x00001020
			  >;
		};

		pinctrl_wifi: wifigrp {
			fsl,pins = <
				MX6UL_PAD_ENET2_TX_CLK__GPIO2_IO14	00001020
			>;
		};
		
		pinctrl_ecspi1: ecspi1grp {
			fsl,pins = <
				MX6UL_PAD_LCD_DATA20__ECSPI1_SCLK	0x00001020
				MX6UL_PAD_LCD_DATA21__ECSPI1_SS0	0x00001020
				MX6UL_PAD_LCD_DATA22__ECSPI1_MOSI	0x00001020
				MX6UL_PAD_LCD_DATA23__ECSPI1_MISO	0x00001020
			  >;
		};
		
		pinctrl_ecspi2: ecspi2grp {
			fsl,pins = <
				MX6UL_PAD_CSI_DATA00__ECSPI2_SCLK	0x00001020
				MX6UL_PAD_CSI_DATA01__ECSPI2_SS0	0x00001020
				MX6UL_PAD_UART5_RX_DATA__ECSPI2_MISO	0x00001020
				MX6UL_PAD_UART5_TX_DATA__ECSPI2_MOSI	0x00001020
			  >;
		};


		pinctrl_enet1: enet1grp {
			fsl,pins = <
				MX6UL_PAD_GPIO1_IO07__ENET1_MDC	0x000010A0
				MX6UL_PAD_ENET2_RX_DATA0__ENET1_MDIO	0x00001820
				MX6UL_PAD_ENET1_RX_DATA0__ENET1_RDATA00	0x00001088
				MX6UL_PAD_ENET1_RX_DATA1__ENET1_RDATA01	0x00001088
 				MX6UL_PAD_ENET1_TX_CLK__ENET1_REF_CLK1	0x00001088
				MX6UL_PAD_ENET1_RX_EN__ENET1_RX_EN	0x00001088
				MX6UL_PAD_ENET1_RX_ER__ENET1_RX_ER	0x00001088
				MX6UL_PAD_ENET1_TX_DATA0__ENET1_TDATA00	0x00001088
				MX6UL_PAD_ENET1_TX_DATA1__ENET1_TDATA01	0x00001088
				MX6UL_PAD_ENET1_TX_EN__ENET1_TX_EN	0x00001088
			>;
		};

                pinctrl_i2c1: i2c1grp {
                        fsl,pins = <
                                MX6UL_PAD_CSI_PIXCLK__I2C1_SCL  0x00001820
                                MX6UL_PAD_GPIO1_IO03__I2C1_SDA  0x00001820
                        >;
                };

		pinctrl_i2c2: i2c2grp {
			fsl,pins = <
				MX6UL_PAD_CSI_HSYNC__I2C2_SCL	0x4001b8b0
				MX6UL_PAD_CSI_VSYNC__I2C2_SDA	0x4001b8b0
			>;
		};

		pinctrl_i2c4: i2c4grp {
			fsl,pins = <
				MX6UL_PAD_LCD_DATA03__I2C4_SCL	0x00001820
				MX6UL_PAD_LCD_DATA02__I2C4_SDA	0x00001820
			>;
		};

		pinctrl_pwm2: pwm2grp {
			fsl,pins = <
				MX6UL_PAD_LCD_DATA01__PWM2_OUT	0x00009030
			>;
		};

		pinctrl_pwm6: pwm6grp {
			fsl,pins = <
				MX6UL_PAD_JTAG_TDI__PWM6_OUT	0x00007030
			>;
		};

		pinctrl_qspi: qspigrp {
			fsl,pins = <
				MX6UL_PAD_NAND_READY_B__QSPI_A_DATA00	0x00009020
				MX6UL_PAD_NAND_CE0_B__QSPI_A_DATA01	0x00001020
				MX6UL_PAD_NAND_CE1_B__QSPI_A_DATA02	0x00001020
				MX6UL_PAD_NAND_CLE__QSPI_A_DATA03	0x00001020
				MX6UL_PAD_NAND_WP_B__QSPI_A_SCLK	0x00001020
				MX6UL_PAD_NAND_DQS__QSPI_A_SS0_B	0x00001020
			>;
		};
		
		pinctrl_sjc: sjcgrp {
			fsl,pins = <
				MX6UL_PAD_JTAG_MOD__SJC_MOD	0x00003020
				MX6UL_PAD_JTAG_TCK__SJC_TCK	0x00003020
				MX6UL_PAD_JTAG_TDO__SJC_TDO	0x00009021
				MX6UL_PAD_JTAG_TMS__SJC_TMS	0x00007020
				MX6UL_PAD_JTAG_TRST_B__SJC_TRSTB	0x00007020
			>;
		};

		pinctrl_uart1: uart1grp {
			fsl,pins = <
				MX6UL_PAD_UART1_CTS_B__UART1_DCE_CTS	0x00001020
				MX6UL_PAD_UART1_RTS_B__UART1__DCE_RTS	0x00001020
				MX6UL_PAD_UART1_RX_DATA__UART1_DCE_RX	0x00001020
				MX6UL_PAD_UART1_TX_DATA__UART1_DCE_TX	0x00001020
			>;
		};

		pinctrl_uart2: uart2grp {
			fsl,pins = <
				MX6UL_PAD_UART2_RX_DATA__UART2_DCE_RX	0x00001020
				MX6UL_PAD_UART2_TX_DATA__UART2_DCE_TX	0x00001020
			>;
		};

		pinctrl_uart3: uart3grp {
			fsl,pins = <
				MX6UL_PAD_UART3_CTS_B__UART3_DCE_CTS	0x00001020
				MX6UL_PAD_UART3_RTS_B__UART3_DCE_RTS	0x00001020
				MX6UL_PAD_UART3_RX_DATA__UART3_DCE_RX	0x00001020
				MX6UL_PAD_UART3_TX_DATA__UART3_DCE_TX	0x00001020
			>;
		};

		pinctrl_uart4: uart4grp {
			fsl,pins = <
				MX6UL_PAD_UART4_RX_DATA__UART4_DCE_RX	0x00001020
				MX6UL_PAD_UART4_TX_DATA__UART4_DCE_TX	0x00001020
			>;
		};

		pinctrl_uart5: uart5grp {
			fsl,pins = <
				MX6UL_PAD_GPIO1_IO04__UART5_DCE_TX	0x00001020
				MX6UL_PAD_GPIO1_IO05__UART5_DCE_RX	0x00001020
			>;
		};

		pinctrl_uart6: uart6grp {
			fsl,pins = <
				MX6UL_PAD_CSI_MCLK__UART6_DCE_TX	0x00001020
				MX6UL_PAD_ENET2_RX_DATA1__UART6_DCE_RX	0x00001020
			>;
		};

		pinctrl_uart7: uart7grp {
			fsl,pins = <
				MX6UL_PAD_ENET2_RX_EN__UART7_DCE_TX	0x00001020
				MX6UL_PAD_ENET2_TX_DATA0__UART7_DCE_RX	0x00001020
			>;
		};

		pinctrl_uart8: uart8grp {
			fsl,pins = <
				MX6UL_PAD_ENET2_TX_DATA1__UART8_DCE_TX	0x00001020
				MX6UL_PAD_ENET2_TX_EN__UART8_DCE_RX	0x00001020
			>;
		};
		
		pinctrl_usdhc1: usdhc1grp {
			fsl,pins = <
				MX6UL_PAD_SD1_CLK__USDHC1_CLK	0x000010A0
				MX6UL_PAD_SD1_CMD__USDHC1_CMD	0x000090A0
				MX6UL_PAD_SD1_DATA0__USDHC1_DATA0	0x000090A0
				MX6UL_PAD_SD1_DATA1__USDHC1_DATA1	0x000090A0
				MX6UL_PAD_SD1_DATA2__USDHC1_DATA2	0x000090A0
				MX6UL_PAD_SD1_DATA3__USDHC1_DATA3	0x000090A0
			>;
		};

		pinctrl_usdhc2: usdhc2grp {
			fsl,pins = <
				MX6UL_PAD_NAND_DATA00__USDHC2_DATA0	0x000090A0
				MX6UL_PAD_NAND_DATA01__USDHC2_DATA1	0x000010A0
				MX6UL_PAD_NAND_DATA02__USDHC2_DATA2	0x000010A0
				MX6UL_PAD_NAND_DATA03__USDHC2_DATA3	0x000010A0
				MX6UL_PAD_NAND_DATA04__USDHC2_DATA4	0x000010A0
				MX6UL_PAD_NAND_DATA05__USDHC2_DATA5	0x000010A0
				MX6UL_PAD_NAND_DATA06__USDHC2_DATA6	0x000010A0
				MX6UL_PAD_NAND_DATA07__USDHC2_DATA7	0x000010A0
				MX6UL_PAD_NAND_RE_B__USDHC2_CLK	0x000010A0
				MX6UL_PAD_NAND_WE_B__USDHC2_CMD	0x000090A0
			>;
		};
		
		pinctrl_wdog: wdoggrp {
			fsl,pins = <
				MX6UL_PAD_ENET2_RX_ER__WDOG1_WDOG_ANY	0x000010B0
			>;
		};


	};
};

&pwm2 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_pwm2>;
	status = "okay";
	clocks = <&clks IMX6UL_CLK_PWM2>,
					 <&clks IMX6UL_CLK_PWM2>;
};

&pwm6 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_pwm6>;
	status = "okay";
	clocks = <&clks IMX6UL_CLK_PWM6>,
					 <&clks IMX6UL_CLK_PWM6>;
};

&uart1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart1>;
	status = "okay";
};

&uart2 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart2>;
	status = "okay";
};

&uart3 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart3>;
	status = "okay";
};

&usbotg1 {
	dr_mode = "otg";
	srp-disable;
	hnp-disable;
	adp-disable;
	status = "okay";
};

&usbotg2 {
	dr_mode = "host";
	disable-over-current;
	status = "okay";
};

&usbphy1 {
	tx-d-cal = <0x5>;
};

&usbphy2 {
	tx-d-cal = <0x5>;
};

&wdog1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_wdog>;
	fsl,wdog_b;
};

&usdhc1 {
        pinctrl-names = "default";
        pinctrl-0 = <&pinctrl_usdhc1 &pinctrl_wifi>;
        no-1-8-v;
        non-removable;
        bus-width = <4>;
        pm-ignore-notify;
        status = "okay";
};

&usdhc2 {
        pinctrl-names = "default";
        pinctrl-0 = <&pinctrl_usdhc2>;
        no-1-8-v;
        non-removable;
        bus-width = <8>;
        keep-power-in-suspend;
        enable-sdio-wakeup;
        status = "okay";
};

