/*
 * Copyright (C) 2017 Rigado
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

/dts-v1/;

#include <dt-bindings/input/input.h>
#include "imx6ul.dtsi"

/ {
	model = "Rigado Vesta rad Board ";
	compatible = "fsl,vesta-rad", "fsl,imx6ul";

	chosen {
		stdout-path = &uart2;
	};

	memory {
		reg = <0x80000000 0x8000000>;
	};

	reserved-memory {
		#address-cells = <1>;
		#size-cells = <1>;
		ranges;

		linux,cma {
			compatible = "shared-dma-pool";
			reusable;
			size = <0x200000>;
			linux,cma-default;
		};
	};

	regulators {
		compatible = "simple-bus";
		#address-cells = <1>;
		#size-cells = <0>;

		reg_can_3v3: regulator@0 {
			compatible = "regulator-fixed";
			reg = <0>;
			regulator-name = "can-3v3";
			regulator-min-microvolt = <3300000>;
			regulator-max-microvolt = <3300000>;
		};

		reg_vref_3v: regulator@1 {
			  compatible = "regulator-fixed";
			  regulator-name = "vref-3v";
			  regulator-min-microvolt = <3075000>;
			  regulator-max-microvolt = <3075000>;
	  	};
	};
};

&cpu0 {
	arm-supply = <&reg_arm>;
	soc-supply = <&reg_soc>;
};

&fec1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_enet1>;
	phy-mode = "rmii";
	phy-handle = <&ethphy0>;
	status = "okay";

	mdio {
		#address-cells = <1>;
		#size-cells = <0>;

		ethphy0: ethernet-phy@1 {
			compatible = "ethernet-phy-ieee802.3-c22";
			reg = <1>;
		};

		ethphy1: ethernet-phy@2 {
			compatible = "ethernet-phy-ieee802.3-c22";
			reg = <2>;
		};
	};
};

&gpc {
	fsl,cpu_pupscr_sw2iso = <0x1>;
	fsl,cpu_pupscr_sw = <0x0>;
	fsl,cpu_pdnscr_iso2sw = <0x1>;
	fsl,cpu_pdnscr_iso = <0x1>;
	fsl,ldo-bypass = <0>; /* DCDC, ldo-enable */
};

&i2c1 {
	clock-frequency = <100000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_i2c1>;
	status = "okay";
};

&adc1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_adc1>;
	status = "okay";
	vref-supply = <&reg_vref_3v>;
};

&iomuxc {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_hog_1>;
	imx6ul-evk {
		pinctrl_hog_1: hoggrp-1 {
			fsl,pins = <
				MX6UL_PAD_GPIO1_IO02__GPIO1_IO02	0x80000000 /* GPIO Exp */
				MX6UL_PAD_GPIO1_IO06__GPIO1_IO06	0x80000000 /* PMIC INT N */
				MX6UL_PAD_GPIO1_IO08__GPIO1_IO08	0x80000000 /* SPDIF OUT */
				MX6UL_PAD_CSI_DATA02__GPIO4_IO23	0x80000000 /* BT GPIO */
				MX6UL_PAD_CSI_DATA03__GPIO4_IO24	0x17059 /* BT RST N */
				MX6UL_PAD_CSI_DATA04__GPIO4_IO25	0x17059 /* ENET1 RST N */
				MX6UL_PAD_CSI_DATA05__GPIO4_IO26	0x80000000 /* RST BTN */
				MX6UL_PAD_CSI_DATA06__GPIO4_IO27	0x80000000 /* BT INT N */
				MX6UL_PAD_CSI_DATA07__GPIO4_IO28	0x80000000 /* ENET1 INT N */
				MX6UL_PAD_NAND_ALE__USDHC2_RESET_B	0x17059 /* eMMC RST N */
				MX6UL_PAD_BOOT_MODE0__GPIO5_IO10    0x80000000 /* user button */
				MX6UL_PAD_LCD_DATA20__GPIO3_IO25	0xb0 /* BLE_INT_N1 */
				MX6UL_PAD_LCD_DATA21__GPIO3_IO26	0xb0 /* BLE_INT_N2 */
				MX6UL_PAD_LCD_DATA22__GPIO3_IO27	0xb0 /* BLE_INT_N3 */
				MX6UL_PAD_LCD_DATA23__GPIO3_IO28	0xb0 /* BLE_INT_N4 */
				MX6UL_PAD_LCD_DATA16__GPIO3_IO21	0xb0 /* BLE_RST_N1 */
				MX6UL_PAD_LCD_DATA17__GPIO3_IO22	0xb0 /* BLE_RST_N2 */
				MX6UL_PAD_LCD_DATA18__GPIO3_IO23	0xb0 /* BLE_RST_N2 */
				MX6UL_PAD_LCD_DATA19__GPIO3_IO24	0xb0 /* BLE_RST_N3 */
			>;
		};

		pinctrl_adc1: adc1grp {
			fsl,pins = <
				MX6UL_PAD_GPIO1_IO00__GPIO1_IO00 		0xb0
			  	MX6UL_PAD_GPIO1_IO01__GPIO1_IO01		0xb0
			  >;
		};

		pinctrl_wifi: wifigrp {
			fsl,pins = <
				MX6UL_PAD_ENET2_TX_CLK__GPIO2_IO14 0x03029
			>;
		};
		
		pinctrl_enet1: enet1grp {
			fsl,pins = <
				MX6UL_PAD_ENET2_RX_DATA0__ENET1_MDIO	0x1b0b0
				MX6UL_PAD_GPIO1_IO07__ENET1_MDC			0x1b0b0
				MX6UL_PAD_ENET1_RX_DATA0__ENET1_RDATA00	0x1b0b0
				MX6UL_PAD_ENET1_RX_DATA1__ENET1_RDATA01	0x1b0b0
				MX6UL_PAD_ENET1_RX_EN__ENET1_RX_EN		0x1b0b0
				MX6UL_PAD_ENET1_RX_ER__ENET1_RX_ER		0x1b0b0
				MX6UL_PAD_ENET1_TX_DATA0__ENET1_TDATA00	0x1b0b0
				MX6UL_PAD_ENET1_TX_DATA1__ENET1_TDATA01	0x1b0b0
				MX6UL_PAD_ENET1_TX_EN__ENET1_TX_EN		0x1b0b0
				MX6UL_PAD_ENET1_TX_CLK__ENET1_REF_CLK1	0x4001b031
			>;
		};

		pinctrl_i2c1: i2c1grp {
			fsl,pins = <
				MX6UL_PAD_CSI_PIXCLK__I2C1_SCL 0x4001b8b0
				MX6UL_PAD_GPIO1_IO03__I2C1_SDA 0x4001b8b0
			>;
		};

		pinctrl_pwm2: pwm2grp {
			fsl,pins = <
				MX6UL_PAD_GPIO1_IO09__PWM2_OUT   0x110b0
			>;
		};

		pinctrl_pwm6: pwm6grp {
			fsl,pins = <
				MX6UL_PAD_JTAG_TDI__PWM6_OUT   0x110b0
			>;
		};

		pinctrl_qspi: qspigrp {
			fsl,pins = <
				MX6UL_PAD_NAND_WP_B__QSPI_A_SCLK      0x70a1
				MX6UL_PAD_NAND_READY_B__QSPI_A_DATA00 0x70a1
				MX6UL_PAD_NAND_CE0_B__QSPI_A_DATA01   0x70a1
				MX6UL_PAD_NAND_CE1_B__QSPI_A_DATA02   0x70a1
				MX6UL_PAD_NAND_CLE__QSPI_A_DATA03     0x70a1
				MX6UL_PAD_NAND_DQS__QSPI_A_SS0_B      0x70a1
			>;
		};
		
		pinctrl_uart1: uart1grp {
			fsl,pins = <
				MX6UL_PAD_UART1_TX_DATA__UART1_DCE_TX 0x1b0b1
				MX6UL_PAD_UART1_RX_DATA__UART1_DCE_RX 0x1b0b1
				MX6UL_PAD_UART1_RTS_B__UART1_DCE_RTS  0x1b0b1
				MX6UL_PAD_UART1_CTS_B__UART1_DCE_CTS  0x1b0b1
			>;
		};

		pinctrl_uart2: uart2grp {
			fsl,pins = <
				MX6UL_PAD_UART2_TX_DATA__UART2_DCE_TX	0x1b0b1
				MX6UL_PAD_UART2_RX_DATA__UART2_DCE_RX	0x1b0b1
			>;
		};

		pinctrl_uart3: uart3grp {
			fsl,pins = <
				MX6UL_PAD_UART3_TX_DATA__UART3_DCE_TX 0x1b0b1
				MX6UL_PAD_UART3_RX_DATA__UART3_DCE_RX 0x1b0b1
				MX6UL_PAD_UART3_CTS_B__UART3_DCE_CTS 0x1b0b1
				MX6UL_PAD_UART3_RTS_B__UART3_DCE_RTS 0x1b0b1
			>;
		};

		pinctrl_uart4: uart4grp {
			fsl,pins = <
				MX6UL_PAD_UART4_TX_DATA__UART4_DCE_TX	0x1b0b1
				MX6UL_PAD_UART4_RX_DATA__UART4_DCE_RX	0x1b0b1
				MX6UL_PAD_LCD_HSYNC__UART4_DCE_CTS		0x1b0b1
				MX6UL_PAD_LCD_VSYNC__UART4_DCE_RTS		0x1b0b1
			>;
		};

		pinctrl_uart5: uart5grp {
			fsl,pins = <
				MX6UL_PAD_GPIO1_IO04__UART5_DCE_TX 0x1b0b1
				MX6UL_PAD_GPIO1_IO05__UART5_DCE_RX 0x1b0b1
			>;
		};

		pinctrl_uart6: uart6grp {
			fsl,pins = <
				MX6UL_PAD_ENET2_RX_DATA1__UART6_DCE_RX	0x1b0b1
				MX6UL_PAD_CSI_MCLK__UART6_DCE_TX		0x1b0b1
				MX6UL_PAD_CSI_HSYNC__UART6_DCE_CTS		0x1b0b1
				MX6UL_PAD_CSI_VSYNC__UART6_DCE_RTS		0x1b0b1
			>;
		};

		pinctrl_uart7: uart7grp {
			fsl,pins = <
				MX6UL_PAD_ENET2_TX_DATA0__UART7_DCE_RX 	0x1b0b1
				MX6UL_PAD_ENET2_RX_EN__UART7_DCE_TX 	0x1b0b1
				MX6UL_PAD_LCD_DATA06__UART7_DCE_CTS 	0x1b0b1
				MX6UL_PAD_LCD_DATA07__UART7_DCE_RTS 	0x1b0b1
			>;
		};

		pinctrl_uart8: uart8grp {
			fsl,pins = <
				MX6UL_PAD_ENET2_TX_EN__UART8_DCE_RX		0x1b0b1
				MX6UL_PAD_ENET2_TX_DATA1__UART8_DCE_TX	0x1b0b1
				MX6UL_PAD_LCD_DATA04__UART8_DCE_CTS		0x1b0b1
				MX6UL_PAD_LCD_DATA05__UART8_DCE_RTS		0x1b0b1
			>;
		};
		
		pinctrl_usdhc1: usdhc1grp {
			fsl,pins = <
				MX6UL_PAD_SD1_CMD__USDHC1_CMD     0x17059
				MX6UL_PAD_SD1_CLK__USDHC1_CLK     0x10071
				MX6UL_PAD_SD1_DATA0__USDHC1_DATA0 0x17059
				MX6UL_PAD_SD1_DATA1__USDHC1_DATA1 0x17059
				MX6UL_PAD_SD1_DATA2__USDHC1_DATA2 0x17059
				MX6UL_PAD_SD1_DATA3__USDHC1_DATA3 0x17059
			>;
		};

		pinctrl_usdhc2: usdhc2grp {
			fsl,pins = <
				MX6UL_PAD_NAND_RE_B__USDHC2_CLK     0x10069
				MX6UL_PAD_NAND_WE_B__USDHC2_CMD     0x17059
				MX6UL_PAD_NAND_DATA00__USDHC2_DATA0 0x17059
				MX6UL_PAD_NAND_DATA01__USDHC2_DATA1 0x17059
				MX6UL_PAD_NAND_DATA02__USDHC2_DATA2 0x17059
				MX6UL_PAD_NAND_DATA03__USDHC2_DATA3 0x17059
				MX6UL_PAD_NAND_DATA04__USDHC2_DATA4 0x17059
				MX6UL_PAD_NAND_DATA05__USDHC2_DATA5 0x17059
				MX6UL_PAD_NAND_DATA06__USDHC2_DATA6 0x17059
				MX6UL_PAD_NAND_DATA07__USDHC2_DATA7 0x17059
			>;
		};
		
		pinctrl_wdog: wdoggrp {
			fsl,pins = <
				MX6UL_PAD_ENET2_RX_ER__WDOG1_WDOG_ANY    0x30b0
			>;
		};
	};
};

&pwm2 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_pwm2>;
	status = "okay";
	clocks = <&clks IMX6UL_CLK_PWM2>,
					 <&clks IMX6UL_CLK_PWM2>;
};

&pwm6 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_pwm6>;
	status = "okay";
	clocks = <&clks IMX6UL_CLK_PWM6>,
					 <&clks IMX6UL_CLK_PWM6>;
};

&qspi {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_qspi>;
	status = "okay";
	ddrsmp=<0>;

	flash0: mx25l25635e@0 {
		#address-cells = <1>;
		#size-cells = <1>;
		compatible = "micron,mx25l25645g";
		spi-max-frequency = <29000000>;
		spi-nor,ddr-quad-read-dummy = <6>;
		reg = <0>;
		partition@0 {
			/* This location must not be altered  */
			/* 1MB uboot */
			reg = <0 0x100000>;
			label = "u-boot";
		};
		partition@1 {
			/* This location must not be altered  */
			/* 1MB persistent data */
			reg = <0x100000 0x100000>;
			label = "persistent";
		};
		partition@2 {
			/* This location must not be altered  */
			/* 7MB rootfs */
			reg = <0x200000 0x700000>;
			label = "kernel";
		};
		partition@3 {
			/* This location must not be altered  */
			/* 1MB for edata */
			reg = <0x900000 0x1800000>;
			label = "rootfs";
		};
	};
};

&uart1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart1>;
	fsl,uart-has-rtscts;
	status = "okay";
};

&uart2 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart2>;
	status = "okay";
};

&uart3 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart3>;
	fsl,uart-has-rtscts;
	status = "okay";
};

&uart4 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart4>;
	fsl,uart-has-rtscts;
	status = "okay";
};

&uart6 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart6>;
	fsl,uart-has-rtscts;
	status = "okay";
};

&uart7 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart7>;
	fsl,uart-has-rtscts;
	status = "okay";
};

&uart8 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart8>;
	fsl,uart-has-rtscts;
	status = "okay";
};

&usbotg1 {
    dr_mode = "otg";
    srp-disable;
    hnp-disable;
    adp-disable;
    status = "okay";
};

&usbotg2 {
	dr_mode = "host";
	disable-over-current;
	status = "okay";
};

&usbphy1 {
	tx-d-cal = <0x5>;
};

&usbphy2 {
	tx-d-cal = <0x5>;
};

&wdog1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_wdog>;
	fsl,wdog_b;
};
