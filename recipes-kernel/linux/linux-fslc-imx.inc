#Do not include zImage in rootfs
RDEPENDS_kernel-base = ""

#Vesta specific files
SRC_URI += "file://defconfig \ 
			file://device-tree/vesta-100.dts \
			file://device-tree/vesta-100-mfg.dts \
			file://device-tree/vesta-200.dts \
			file://device-tree/vesta-200-mfg.dts \
			file://device-tree/vesta-300.dts \
			file://device-tree/vesta-300-mfg.dts \
			file://device-tree/vesta-400.dts \
			file://device-tree/vesta-400-mfg.dts \
			file://device-tree/vesta-500.dts \
			file://device-tree/vesta-500-mfg.dts \
			file://device-tree/vesta-64M-nor.dts \
			file://device-tree/vesta-64M-nor-mfg.dts \
			file://device-tree/vesta-rad-mfg.dts \
			file://device-tree/vesta-rad-test-mfg.dts \
			file://device-tree/vesta-rad.dts \
			file://device-tree/vesta-rad-test.dts \
			file://device-tree/vesta.dtsi \
			file://device-tree/310-00110-0003.dts \
			file://device-tree/vesta-mfg.dtsi \
			file://patches/0001-QCA6234-driver.patch \
			file://patches/0001-bluebourne.patch \
			file://patches/0001-Added-Quectel.patch \
			file://patches/0001-nor-flash-fix.patch \
			file://patches/0001-overlay-manager.patch \
			file://patches/0001-backports-to-support-qcacld-v2.3.3-9377-driver.patch \
"

S = "${WORKDIR}/git"

do_configure_append() {
	# Add device-tree sources
	cp ${WORKDIR}/device-tree/vesta.dtsi ${S}/arch/${ARCH}/boot/dts
	cp ${WORKDIR}/device-tree/vesta-mfg.dtsi ${S}/arch/${ARCH}/boot/dts
	cp ${WORKDIR}/device-tree/vesta-100.dts ${S}/arch/${ARCH}/boot/dts
	cp ${WORKDIR}/device-tree/vesta-100-mfg.dts ${S}/arch/${ARCH}/boot/dts
	cp ${WORKDIR}/device-tree/vesta-200.dts ${S}/arch/${ARCH}/boot/dts
	cp ${WORKDIR}/device-tree/vesta-200-mfg.dts ${S}/arch/${ARCH}/boot/dts
	cp ${WORKDIR}/device-tree/vesta-300.dts ${S}/arch/${ARCH}/boot/dts
	cp ${WORKDIR}/device-tree/vesta-300-mfg.dts ${S}/arch/${ARCH}/boot/dts
	cp ${WORKDIR}/device-tree/vesta-400.dts ${S}/arch/${ARCH}/boot/dts
	cp ${WORKDIR}/device-tree/vesta-400-mfg.dts ${S}/arch/${ARCH}/boot/dts
	cp ${WORKDIR}/device-tree/vesta-500.dts ${S}/arch/${ARCH}/boot/dts
	cp ${WORKDIR}/device-tree/vesta-500-mfg.dts ${S}/arch/${ARCH}/boot/dts
	cp ${WORKDIR}/device-tree/vesta-64M-nor.dts ${S}/arch/${ARCH}/boot/dts
	cp ${WORKDIR}/device-tree/vesta-64M-nor-mfg.dts ${S}/arch/${ARCH}/boot/dts
	cp ${WORKDIR}/device-tree/vesta-rad-mfg.dts ${S}/arch/${ARCH}/boot/dts
	cp ${WORKDIR}/device-tree/vesta-rad-test-mfg.dts ${S}/arch/${ARCH}/boot/dts
	cp ${WORKDIR}/device-tree/vesta-rad.dts ${S}/arch/${ARCH}/boot/dts
	cp ${WORKDIR}/device-tree/vesta-rad-test.dts ${S}/arch/${ARCH}/boot/dts
	cp ${WORKDIR}/device-tree/310-00110-0003.dts ${S}/arch/${ARCH}/boot/dts
}

do_install_append() {
	install -d ${D}${base_libdir}/firmware/overlays
	install -m 755 ${B}/arch/${ARCH}/boot/dts/310-00110-0003.dtb ${D}${base_libdir}/firmware/overlays/310-00110-0003.dtbo
}

FILES_kernel-base += " \
    ${base_libdir}/firmware/overlays/310-00110-0003.dtbo \
"
