# Copyright (C) 2015, 2016 O.S. Systems Software LTDA.
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "Linux recipe for Rigado Vesta board"

require recipes-kernel/linux/linux-imx.inc
require recipes-kernel/linux/linux-dtb.inc

DEPENDS += "lzop-native bc-native"
RDEPENDS_kernel-base = ""

PV .= "4.1-1.0+git${SRCPV}"

SRCBRANCH = "master"

#Always update SRCREV based on your last commit
SRCREV = "c383b635a7a1d75c78e9ed75b3a676fb4e7d7680"

SRC_URI = "git://git.rigado.com/vesta/linux-fslc-imx-4.1-1.0.git;protocol=https;branch=${SRCBRANCH}; \ 
           file://defconfig"

COMPATIBLE_MACHINE = "(vesta|imx6ul)"
