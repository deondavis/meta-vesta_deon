DESCRIPTION = "Library for calculating CRC16."
HOMEPAGE = "http://code.google.com/p/pycrc16"
LICENSE = "LGPLv3"
LIC_FILES_CHKSUM = "file://COPYING.txt;md5=38138baa100d7259934590850bc0406e"

SRC_URI[md5sum] = "3cb6a4f5fd10a58c09792e321cc467e5"

inherit pypi setuptools


