DESCRIPTION = "A python module to simplify the process of getting log data to Initial State's platform."
HOMEPAGE = "https://www.initialstate.com/"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://PKG-INFO;md5=cdcdc64931210536ccaf62a037978f76"

SRC_URI[md5sum] = "e7fb5473b3bfa06d3929f7a82668ec5d"

inherit pypi setuptools


