DESCRIPTION = "Virtual package for MySQL-python."
HOMEPAGE = "https://github.com/valhallasw/virtual-mysql-pypi-package"
LICENSE = "GPL-1.0"
LIC_FILES_CHKSUM = "file://PKG-INFO;md5=a547ee5ee7258a0a5a65c717a4a284c2"

SRC_URI[md5sum] = "17bdc7a8a945c590a0a0ccdd2d8010fd"

inherit pypi setuptools


