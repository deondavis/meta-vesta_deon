DESCRIPTION = "This code provides a client class which enable applications to connect to an MQTT broker to publish messages, and to subscribe to topics and receive published messages. It also provides some helper functions to make publishing one off messages to an MQTT server very straightforward."
HOMEPAGE = "http://eclipse.org/paho"
LICENSE = "EPL-1.0"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=ce486fbc20f569d6378ec96af2e0e86f"

SRC_URI[md5sum] = "2cc27d8b369700b1fc99325add0dadd2"

inherit pypi setuptools


