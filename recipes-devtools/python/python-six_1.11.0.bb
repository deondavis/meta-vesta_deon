DESCRIPTION = "Six is a Python 2 and 3 compatibility library. It provides utility functions for smoothing over the differences between the Python versions with the goal of writing Python code that is compatible on both Python versions."
HOMEPAGE = "http://pypi.python.org/pypi/six/"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=ce486fbc20f569d6378ec96af2e0e86f"

SRC_URI[md5sum] = "d12789f9baf7e9fb2524c0c64f1773f8"

inherit pypi setuptools


