DESCRIPTION = "Python Serial Port Extension"
HOMEPAGE = "https://github.com/pyserial/pyserial"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=9d7ca5f1caf870e000fcbada654612d7"

SRC_URI[md5sum] = "ed6183b15519a0ae96675e9c3330c69b"

inherit pypi setuptools

