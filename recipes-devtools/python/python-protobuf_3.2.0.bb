DESCRIPTION = "Protocol Buffers are Google’s data interchange format"
HOMEPAGE = "https://developers.google.com/protocol-buffers/"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://PKG-INFO;md5=e864f26731644725d680798a063b9b78"

SRC_URI[md5sum] = "f1daa5fee5de4e61e757a5f97e6aa7fd"

inherit pypi setuptools
