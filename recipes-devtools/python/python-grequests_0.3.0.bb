DESCRIPTION = "GRequests allows you to use Requests with Gevent to make asynchronous HTTP Requests easily."
HOMEPAGE = "https://github.com/kennethreitz/grequests/"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://PKG-INFO;md5=b24e5e016b71ea7d0727cc7b0e877aa5"

SRC_URI[md5sum] = "8792b38aa182051cabba34c3ca57fc4b"

inherit pypi setuptools
