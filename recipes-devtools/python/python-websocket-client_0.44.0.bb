DESCRIPTION = "websocket-client module is WebSocket client for python. This provide the low level APIs for WebSocket. All APIs are the synchronous functions"
HOMEPAGE = "https://github.com/websocket-client/websocket-client.git"
LICENSE = "LGPL-3.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=18b09a20dd186af4fd60f1a08311628c"

SRC_URI[md5sum] = "73d87aa16a2212da448b30aca9c5bf3b"

inherit pypi setuptools

PYPI_PACKAGE = "websocket_client"
