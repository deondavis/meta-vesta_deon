DESCRIPTION = "Dweepy is a simple Python client for dweet.io."
HOMEPAGE = "https://github.com/paddycarey/dweepy"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=66c0e0f1d36206e62739ba8d54071924"

SRC_URI[md5sum] = "5fff6fd687a0954f86af1876a96e814c"

inherit pypi setuptools


