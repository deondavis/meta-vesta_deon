# This class recipe is a hack added to work around the fact that 
# Poky's morty release is missing a few components needed
# to build the meta-java layer.  The components in question are present
# in the Poky tip-of-tree, so if the vesta project eventually switches
# to a later version of Poky, this file may no longer be needed.

do_install[postfuncs] += "install_relative_symlinks"

python install_relative_symlinks () {
    def replace_absolute_symlinks(basedir, d):
        """
        Walk basedir looking for absolute symlinks and replacing them with relative ones.
        The absolute links are assumed to be relative to basedir
        (compared to make_relative_symlink above which tries to compute common ancestors
        using pattern matching instead)
        """
        for walkroot, dirs, files in os.walk(basedir):
            for file in files + dirs:
                path = os.path.join(walkroot, file)
                if not os.path.islink(path):
                    continue
                link = os.readlink(path)
                if not os.path.isabs(link):
                    continue
                walkdir = os.path.dirname(path.rpartition(basedir)[2])
                base = os.path.relpath(link, walkdir)
                bb.debug(2, "Replacing absolute path %s with relative path %s" % (link, base))
                os.remove(path)
                os.symlink(base, path)

    replace_absolute_symlinks(d.getVar('D',False), d)
}
