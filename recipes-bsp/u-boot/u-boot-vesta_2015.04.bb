# Copyright (C) 2012-2016 O.S. Systems Software LTDA.
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "U-boot recipe for Rigado Vesta board"

require recipes-bsp/u-boot/u-boot.inc
inherit fsl-u-boot-localversion

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://Licenses/README;md5=c7383a594871c03da76b3707929d2919"

LOCALVERSION = "+vesta"

PV = "v2015.04+git${SRCPV}"

PROVIDES += "u-boot"

SRCBRANCH = "master"

#Always update SRCREV based on your last commit
SRCREV = "158adad1ffa25ec2373aed67be4f1b85fbf797d1"

SRC_URI = "git://git.rigado.com/vesta/u-boot-2015.04.git;protocol=https;branch=${SRCBRANCH}"

S = "${WORKDIR}/git"

PACKAGE_ARCH = "${MACHINE_ARCH}"

COMPATIBLE_MACHINE = "(vesta|imx6ul)"

