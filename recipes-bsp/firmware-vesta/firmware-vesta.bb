SUMMARY = "Vesta firmware"
DESCRIPTION = "Vesta wifi bdata.bin, fw.ram.bin file, and abtfilt files"
SECTION = "base"
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://git/firmware/ath6k/LICENSE.qca_firmware;md5=2a397c0e988f4c52d3d526133b617c8d"
LIC_FILES_CHKSUM = "file://git/firmware/bmd300/LICENSE.zephyr_firmware;md5=fa818a259cbed7ce8bc2a22d35a464fc"
LIC_FILES_CHKSUM = "file://git/firmware/bmd300/LICENSE.nrf52_firmware;md5=15abc6de8e66fe3ab60f635700f3179f"

PACKAGE_ARCH = "${MACHINE_ARCH}"
RDEPENDS_${PN} = "dbus-lib libnl libnl-genl bluez5"

#Qualcomm 6234 board data file
SRCBRANCH ?= "master"

SRCREV = "454395538f4d99ac29a0bbad933eff3db1772eba"
SRC_URI = "git://git.rigado.com/vesta/vesta-firmware.git;protocol=https;branch=${SRCBRANCH};destsuffix=${S}/git; \
"
S = "${WORKDIR}"

do_configure() {

    if [ ${DEFAULTTUNE_mx6ul} = "cortexa7thf-neon" ]; then
	cp git/firmware/ath6k/AR6004/hw3.0/abtfilt-hf git/firmware/ath6k/AR6004/hw3.0/abtfilt
	cp git/firmware/ec21/quectel-CM-hf git/firmware/ec21/quectel-CM
	cp git/firmware/ath10k/Qcmbr-hfp git/firmware/ath10k/Qcmbr
    else
	cp git/firmware/ath6k/AR6004/hw3.0/abtfilt-sf git/firmware/ath6k/AR6004/hw3.0/abtfilt
	cp git/firmware/ec21/quectel-CM-sf git/firmware/ec21/quectel-CM
	cp git/firmware/ath10k/Qcmbr-sfp git/firmware/ath10k/Qcmbr
    fi

}

INSANE_SKIP_${PN} = "ldflags \
		already-stripped \
"

do_install() {
    install -d ${D}${sbindir}
    install -m 555 git/firmware/ath6k/AR6004/hw3.0/abtfilt ${D}${sbindir}
    install -m 555 git/firmware/ec21/quectel-CM ${D}${sbindir}
    install -m 555 git/firmware/ath10k/Qcmbr ${D}${sbindir}

    install -d ${D}${base_libdir}/firmware/ath6k/AR6004/hw3.0
    install -m 444 git/firmware/ath6k/LICENSE.qca_firmware ${D}${base_libdir}/firmware
    install -m 555 git/firmware/ath6k/AR6004/hw3.0/fw.ram.bin ${D}${base_libdir}/firmware/ath6k/AR6004/hw3.0
    install -m 444 git/firmware/ath6k/AR6004/hw3.0/bdata.bin ${D}${base_libdir}/firmware/ath6k/AR6004/hw3.0
    
    install -d ${D}${base_libdir}/firmware/bmd300 
    install -m 444 git/firmware/bmd300/LICENSE.nrf52_firmware ${D}${base_libdir}/firmware/bmd300
    install -m 644 git/firmware/bmd300/nrf52832_13.1.0.hex ${D}${base_libdir}/firmware/bmd300
    install -m 644 git/firmware/bmd300/s132_nrf52_4.0.2_softdevice.hex ${D}${base_libdir}/firmware/bmd300

    install -d ${D}${base_libdir}/firmware/bmd300 
    install -m 444 git/firmware/bmd300/LICENSE.zephyr_firmware ${D}${base_libdir}/firmware/bmd300
    install -m 644 git/firmware/bmd300/zephyr_1.9.1_63379fd_s132_3.1.0_rigdfu_3.4.1.hex ${D}${base_libdir}/firmware/bmd300

    install -d ${D}${base_libdir}/firmware/r41z 
    install -m 644 git/firmware/r41z/nxp-thread-r41z.bin ${D}${base_libdir}/firmware/r41z
    
    install -d ${D}${base_libdir}/firmware/ath10k 
    install -m 644 git/firmware/ath10k/bdwlan30.bin ${D}${base_libdir}/firmware/ath10k
    install -m 644 git/firmware/ath10k/otp30.bin ${D}${base_libdir}/firmware/ath10k
    install -m 644 git/firmware/ath10k/qwlan30.bin ${D}${base_libdir}/firmware/ath10k
    install -m 644 git/firmware/ath10k/utf30.bin ${D}${base_libdir}/firmware/ath10k
    install -m 644 git/firmware/ath10k/utfbd30.bin ${D}${base_libdir}/firmware/ath10k
    install -d ${D}${base_libdir}/firmware/ath10k/wlan 
    install -m 644 git/firmware/ath10k/wlan/cfg.dat ${D}${base_libdir}/firmware/ath10k/wlan
    install -m 644 git/firmware/ath10k/wlan/qcom_cfg.ini ${D}${base_libdir}/firmware/ath10k/wlan
}

FILES_${PN} += " \
    ${base_libdir}/firmware/LICENSE.qca_firmware \
    ${base_libdir}/firmware/ath6k/AR6004/hw3.0/fw.ram.bin \
    ${base_libdir}/firmware/ath6k/AR6004/hw3.0/bdata.bin \
    ${sbindir}/abtfilt \
    ${base_libdir}/firmware/bmd300/LICENSE.nrf52_firmware \
    ${base_libdir}/firmware/bmd300/nrf52832_13.1.0.hex \
    ${base_libdir}/firmware/bmd300/s132_nrf52_4.0.2_softdevice.hex \
    ${base_libdir}/firmware/bmd300/LICENSE.zephyr_firmware \
    ${base_libdir}/firmware/bmd300/zephyr_1.9.1_63379fd_s132_3.1.0_rigdfu_3.4.1.hex \
    ${base_libdir}/firmware/r41z/nxp-thread-r41z.bin \
    ${base_libdir}/firmware/ath10k/bdwlan30.bin \
    ${base_libdir}/firmware/ath10k/otp30.bin \
    ${base_libdir}/firmware/ath10k/qwlan30.bin \
    ${base_libdir}/firmware/ath10k/utf30.bin \
    ${base_libdir}/firmware/ath10k/utfbd30.bin \
    ${base_libdir}/firmware/ath10k/wlan/cfg.dat \
    ${base_libdir}/firmware/ath10k/wlan/qcom_cfg.ini \
"

COMPATIBLE_MACHINE = "(vesta|imx6ul)"
