require recipes-vesta/images/vesta-image-minimal.bb

IMAGE_INSTALL_append += " \
    bluez5 \
    bluez5-testtools \
    bluez5-noinst-tools \
    cronie \
    eudev \
    git \
    imx-test \
    iot-gateway-setup \
    iperf3 \
    memtester \
    mtd-utils-ubifs \
    nmon \
    nodejs \
    nodejs-npm \
    node-red-dashboard \
    openjre-8 \
    packagegroup-core-buildessential \
    packagegroup-volansys-gw \
    packagegroup-volansys-gw-demo \
    paho-mqtt \
    git-perltools \
    python \
    python-dev \
    python-pip \
    python3 \
    python3-pip \
    python-compiler \
    python-misc \
    python-multiprocessing \
    rigado-devkit \
    screen \
    smartpm \
    strace \
    tmux \
    usbutils \
    node-red \
    node-red-user \
    node-red-init-script \
    node-red-contrib-aws \
    nginx \
"

