IMAGE_INSTALL_append = " \
    kernel-modules \
    firmware-vesta \
    scripts-vesta \
    linux-firmware-ar3k \
    firmware-qualcomm \
    packagegroup-base-wifi \
    mtd-utils \
    rng-tools \
    i2c-tools \
    avahi-daemon \
    openssh \
    rigtools \
"
