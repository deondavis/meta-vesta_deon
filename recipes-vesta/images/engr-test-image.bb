require recipes-core/images/core-image-minimal.bb

IMAGE_INSTALL_append += " \
    memtester \
    iperf3 \
    packagegroup-base-wifi \
    e2fsprogs \
    dosfstools \
    strace \
    openssh \
    mtd-utils \
    mtd-utils-ubifs \
    rng-tools \
    swupdate \
    kernel-modules \
    firmware-vesta \
    linux-firmware-ar3k \
    firmware-qualcomm \
    scripts-vesta \
    usbutils \
    imx-test \
    bluez5 \
    bluez5-testtools \
    bluez5-noinst-tools \
    i2c-tools \
    curl \
    json-c \
    lua \
    libarchive \
    gnutls \
    rsyslog \
    avahi-daemon \
    python3-pip \
    rigtools \
"

