require recipes-core/images/core-image-minimal.bb

IMAGE_INSTALL_append += " \
    avahi-daemon \
    e2fsprogs \
    gnutls \
    i2c-tools \
    kernel-modules \
    mtd-utils \
    ntp \
    os-release \
    openssh \
    resolvconf \
    rigtools \
    rng-tools \
    rsyslog \
    scripts-vesta \
    swupdate \
"
