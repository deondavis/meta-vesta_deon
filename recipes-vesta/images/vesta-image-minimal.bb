require recipes-core/images/core-image-minimal.bb

IMAGE_INSTALL_append += " \
    avahi-daemon \
    binutils \
    dosfstools \
    e2fsprogs \
    firmware-vesta \
    gemalto-scripts \
    gnutls \
    i2c-tools \
    linux-firmware-ar3k \
    kernel-modules \
    kernel-module-qcacld \
    mtd-utils \
    ntp \
    os-release \
    openocd \
    openssh \
    packagegroup-base-wifi \
    ppp \
    resolvconf \
    rigtools \
    rng-tools \
    rsyslog \
    scripts-vesta \
    swupdate \
"
update_issue() {
    echo "RigOS" > ${IMAGE_ROOTFS}/etc/issue
}

ROOTFS_POSTPROCESS_COMMAND += " update_issue; "
