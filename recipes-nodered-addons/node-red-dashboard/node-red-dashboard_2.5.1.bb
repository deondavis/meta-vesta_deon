SUMMARY = "A dashboard UI for Node-RED."
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=1a71c626ebb67c76cc9003730b1ab94c"

DEPENDS = "nodejs-native"
RDEPENDS_${PN} += " bash node-red"

SECTION = "nodered-package"

S = "${WORKDIR}/git"

SRCREV="2ba847ffdceebf45b20e4a8045a4de47855564e7"
SRC_URI = "git://github.com/node-red/${PN}.git"

inherit npm-install-global

do_configure() {
    :
}

