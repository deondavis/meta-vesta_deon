SUMMARY = "A Node-Red node that will scan for and connect to BlueTooth Low-Energy devices."
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=2ee41112a44fe7014dce33e26468ba93"

DEPENDS = "nodejs-native"
RDEPENDS_${PN} += " bash node-red bluez5 libcap libcap-bin"

SECTION = "nodered-package"

S = "${WORKDIR}/git"

NODERED_USER = "nobody"
NODERED_GROUP = "root"

SRCBRANCH ?= "master"
SRCREV="76877814f1ce7bf4939d6b4c3aa6ce19cd1814c1"
SRC_URI = "git://git.rigado.com/vesta/rigado-devkit.git;protocol=https;branch=${SRCBRANCH}"

inherit npm-install-global

do_configure() {
    :
}

do_install_append() {
    install -d -m 0755 ${D}${datadir}/node-red
    install -m 644 ${S}/examples/dev-kit.json ${D}${datadir}/node-red
    chown -R ${NODERED_USER}:${NODERED_GROUP} ${D}${datadir}/node-red/dev-kit.json
}

FILES_${PN}-staticdev += " \
    ${libdir_native}/node_modules/rigado-devkit/node_modules/usb/build/Release/*/*.a \
    ${libdir_native}/node_modules/rigado-devkit/node_modules/usb/build/Release/*.a \
"
