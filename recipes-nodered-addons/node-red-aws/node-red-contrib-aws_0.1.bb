SUMMARY = "A package that provides a collection of Node-RED nodes for AWS"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=2ee41112a44fe7014dce33e26468ba93"

DEPENDS = "nodejs-native"
RDEPENDS_${PN} += " bash node-red"

SECTION = "nodered-package"

SRCBRANCH ?= "master"

SRC_URI = "git://github.com/daniel-t/${PN}.git;branch=${SRCBRANCH}"
SRCREV = "5ca37bde5dc808c64dd6bb5e92042d618a13db6d"

S = "${WORKDIR}/git"

inherit npm-install-global

do_configure() {
    :
}
