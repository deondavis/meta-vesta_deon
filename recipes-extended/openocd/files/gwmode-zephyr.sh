#!/bin/sh

#This scripts executes the necessary steps to put the
#gateway in zephyr mode. A link is then created to
#gwmode-rm-zephyr.sh to undo these changes and place
#the gateway back intro "normal mode".

#Check to see if a link to gwmode-normal is present,
#if so execute it.
if [ -e /usr/bin/gwmode-normal ]; then
    echo Resetting gateway to normal mode...

    /usr/bin/gwmode-normal
    
    if [ $? -ne 0 ]; then
        #log error to syslog and std error
        logger -s "Failed to restore to normal mode!!!"
        exit 1
    fi

    rm /usr/bin/gwmode-normal
fi

#Program the bmd300 with zephyr binary
/usr/bin/bmd300_program_zephyr.sh

if [ $? -ne 0 ]; then 
    exit 1
fi

#disable hciattach.sh from startup 
update-rc.d -f hciattach.sh remove

#enable start_hci.sh at startup
update-rc.d start_hci.sh start 99 5 .

#System changes have been made, 
#create a link to undo theses changes.
ln -s /usr/bin/gwmode-rm-zephyr.sh /usr/bin/gwmode-normal

/etc/init.d/start_hci.sh

if [ $? -ne 0 ]; then
    #log error to syslog and std error
    logger -s "FAILED to attach hci interface!!!"
    exit 1
else
    echo SUCCESSFULLY attached hci interface.
fi
