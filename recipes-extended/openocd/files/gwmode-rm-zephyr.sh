#!/bin/sh

#This script reverts the changes made by gwmode-zephyr.sh

#disable hciattach.sh from startup 
update-rc.d -f start_hci.sh remove

#enable start_hci.sh at startup
update-rc.d hciattach.sh start 61 S .

#Changes have been undone remove the soft link
rm /usr/bin/gwmode-normal

echo "detaching..."
killall -9 hciattach
sleep 2

/etc/init.d/hciattach.sh

if [ $? -ne 0 ]; then
    #log error to syslog and std error
    logger -s "FAILED to attach hci interface."
else
    echo SUCCESSFULLY attached hci interface.
fi
