#!/bin/sh

#This is a helper script to program the BMD-300 with the nRF52 ble serialization module
bmd300_program.sh -h /lib/firmware/bmd300/s132_nrf52_4.0.2_softdevice.hex -h /lib/firmware/bmd300/nrf52832_13.1.0.hex

if [ $? -eq 0 ]; then
    echo 
    echo "Programming of BMD-300 with nRF52 ble serialization module was SUCCESSFUL"
else
    echo
    echo "Programming of BMD-300 with nRF52 ble serialization module has FAILED"
fi

