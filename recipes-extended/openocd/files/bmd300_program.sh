#!/bin/sh

# bmd00_program.sh version
VERSION=1.0.0

# Print usage information to std out
print_usage() {
    cat <<-EOF
    Usage:
    $(basename $0)  [-v] -h filename.hex [-h filename.hex]... [-b filename.bin,hex_start_address]...
    $(basename $0)  [-v] -b filename.bin,hex_start_address [-h filename.hex]... [-b filename.bin,hex_start_address]...
    
    -v | --version version 

    Examples:
    Single hex file
    $(basename $0) -h bmd300.hex

    Single bin file starting at address 0x1000
    $(basename $0) -b bmd300.bin,0x1000

    Multiple files
    $(basename $0) -b bmd300.bin,0x1000 -h app1.hex -b app2.bin,0x40000000
EOF
}

# validate bin file arguments
# Each argument should be in the format filename,address
#   filename: file name
#   address: must start with 0x followed by no more than 8 hexadecimals example 0x12345678
validate_bin_files ()
{
    for val in ${BIN_FILES[@]}; do
        #parse filename
        filename="${val%%,*}"

        if [ "$filename" != "$val" ]; then
            #get address from string
            address=${val:(( ${#filename} + 1 )) }

            #verify address location starts with 0x
            if [ "${address:0:2}" != "0x" ]; then
                echo "syntax error -m $val"
                echo "Start Address must start with 0x"
                exit 1
            fi

            #verify address is between 0x0 and 0xFFFFFFFF
            if [ "${#address}" -lt "3" ] || [ "${#address}" -gt "10" ] ; then
                echo "syntax error -m $val"
                echo "address must between 0x0 and 0xFFFFFFFF"
                exit 1
            fi
        else
            echo "syntax error -m $val"
            echo "must be in the form -m filename,start_address"
            exit 1
        fi
    done
}

# convert the binary files to hex files
convert_bin_to_hex_files ()
{
    for val in ${BIN_FILES[@]}; do
        #parse filename
        filename="${val%%,*}"

        #get length of filename for indexing
        length=${#filename}
        
        address=${val:$length + 1}

        #convert binary file to hex files
        objcopy -I binary -O ihex --change-address=${address} ${filename} ${filename}.hex

        if [ "$?" -ne "0" ]; then
            exit 1
        fi

        #Add hex files to array
        HEX_FILES+=(${filename}.hex)
    done
}

#Check to see if nrf52 is protected
test_nrf52_protected() {
    echo "Checking BMD300 lock status"
    openocd -f interface/imx-native.cfg -c \
    "transport select swd; source [find target/nrf52.cfg]" -c init \
    -c "dap apreg 1 0x0c" -c exit 2> junk
    APPROTECTSTATUS=$( grep 0x000000 junk )
    rm junk
    if [ $APPROTECTSTATUS = "0x00000001" ] 
    then
        echo "BMD300 NOT PROTECTED"
        return 1
    else
        echo "BMD 300 PROTECTED"
        return 0
    fi
}

test_nrf52_for_protection ()
{
    if test_nrf52_protected
    then
        openocd -f interface/imx-native.cfg \
        -c "transport select swd; source [find target/nrf52.cfg]" \
        -c init \
        -c "dap apreg 1 0x04 0x01; \
        echo Unlocking\ BMD-300...; \
        sleep 30000; \
        dap apreg 1 0x08; \
        dap apreg 1 0x00 0x01; \
        dap apreg 1 0x00 0x00; \
        dap apreg 1 0x04 0x00; \
        dap apreg 1 0x0c" \
        -c exit
    else
        openocd -f interface/imx-native.cfg \
        -c "transport select swd; source [find target/nrf52.cfg]" \
        -c init \
        -c "dap apreg 1 0x04 0x01;" \
        -c exit
        echo "Skipping unlock sequence"
    fi
}

# Check for at least one option
if [ $# -eq 0 ]; then 
    print_usage
    exit 1
fi

#define short an long options here
OPTS=`getopt -o vh:b: --long version -n 'parse-options' -- "$@"`

if [ $? != 0 ]; then 
    echo "Failed parsing options." >&2
    print_usage
    exit 1 
fi

eval set -- "$OPTS"

# increment through the options one at a time
while true; do
  case "$1" in
    -v | --version ) 
        printf "$(basename $0) $VERSION\n"
        exit 0;;
    -h )    HEX_FILES+=("$2"); shift 2;;
    -b )    BIN_FILES+=("$2"); shift 2;;
    -- ) shift; break ;;
    * ) 
        print_usage
        exit 1 ;;
  esac
done

validate_bin_files
convert_bin_to_hex_files

#create cmd string for openocd
for val in ${HEX_FILES[@]}; do
    cmds+=" -c \"program $val verify\""
done

if [ "${cmds}" == "" ]; then
    print_usage
    exit 1
fi

test_nrf52_for_protection

echo "writing files ${HEX_FILES[@]} to BMD 300 . . . ."

#execute openocd command
eval openocd -f interface/imx-native.cfg  \
-c '"transport select swd; source [find target/nrf52.cfg]"' \
-c init -c '"reset init"' -c halt \
$cmds \
-c reset -c exit
