#!/bin/sh

TYPE=$(board_serial_number | cut -b 2-4)

if [ "$TYPE" != "083" ] && [ "$TYPE" != "081" ] && [ "$TYPE" != "077" ] && [ "$TYPE" != "120" ]; then
    logger -s "The Zephyr binary is only supported on Vesta 100B, 200B, 300B or 400B units."
    exit 1
fi

#This is a helper script to program the BMD-300 with the Zephyr hex file
bmd300_program.sh -h /lib/firmware/bmd300/zephyr_1.9.1_63379fd_s132_3.1.0_rigdfu_3.4.1.hex

if [ $? -ne 0 ]; then
    #log error to syslog and std error
    logger -s "Programming of BMD-300 with Zephyr has FAILED"
    exit 1
fi

echo "Programming of BMD-300 with Zephyr was SUCCESSFUL"
