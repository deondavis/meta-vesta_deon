SUMMARY = "Open On-Chip Debugger - configured to run SWD using imx6ul gpio"
HOMEPAGE = "http://openocd.org"
SECTION = "extended"
LICENSE = "GPLv2+"

S = "${WORKDIR}/git"

LIC_FILES_CHKSUM = "file://COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"

DEPENDS = "libusb1"

SRCREV="2c8602ed9f084d6680cec7d0ca1d5dc71c865a5f"

SRC_URI = "git://git.rigado.com/vesta/openocd;protocol=https \
           file://0001-Additional-support-for-nRF52.patch \
	   file://0002-Configure-imx-gpio-for-vesta-SWD-interface.patch \
	   file://bmd300_program.sh \
	   file://bmd300_program_nRF52_PCBLE.sh \
	   file://bmd300_program_zephyr.sh \
	   file://gwmode-zephyr.sh \
	   file://gwmode-rm-zephyr.sh \
	   file://r41z_program.sh \
	   file://load_zephyr \
"

do_configure() {
	"${S}"/bootstrap
        "${S}"/configure --host=armv7 --enable-imx_gpio --disable-stlink \
		--disable-ti-icdi --disable-ulink --disable-usb-blaster-2 \
		--disable-vsllink --disable-osbdm --disable-opendous \
		--disable-aice --disable-usbprog --disable-rlink \
		--disable-armjtagew --disable-cmsis-dap --disable-kitprog \
		--disable-usb-blaster --disable-presto --disable-openjtag \
		--disable-jlink
}

do_install() {
	install -d ${D}/usr/sbin
	install -m 0744 src/openocd ${D}/usr/sbin
	install -d ${D}/usr/share/openocd/scripts
	install -m 0744 tcl/bitsbytes.tcl ${D}/usr/share/openocd/scripts
	install -m 0744 tcl/memory.tcl ${D}/usr/share/openocd/scripts
	install -m 0744 tcl/mmr_helpers.tcl ${D}/usr/share/openocd/scripts
	install -d ${D}/usr/share/openocd/scripts/target
	install -m 0744 tcl/target/nrf52.cfg ${D}/usr/share/openocd/scripts/target
	install -m 0744 tcl/target/kx.cfg ${D}/usr/share/openocd/scripts/target
	install -m 0744 tcl/target/swj-dp.tcl ${D}/usr/share/openocd/scripts/target
	install -d ${D}/usr/share/openocd/scripts/interface
	install -m 0744 tcl/interface/imx-native.cfg ${D}/usr/share/openocd/scripts/interface
	install -d ${D}/usr/share/openocd/scripts/cpu/arm
	install -m 0744 tcl/cpu/arm/arm7tdmi.tcl ${D}/usr/share/openocd/scripts/cpu/arm/
	install -d ${D}${bindir}
	install -m 0744 ${WORKDIR}/bmd300_program.sh ${D}${bindir}
	install -m 0744 ${WORKDIR}/bmd300_program_nRF52_PCBLE.sh ${D}${bindir}
	install -m 0744 ${WORKDIR}/bmd300_program_zephyr.sh ${D}${bindir}
	install -m 0744 ${WORKDIR}/gwmode-zephyr.sh ${D}${bindir}
	install -m 0744 ${WORKDIR}/gwmode-rm-zephyr.sh ${D}${bindir}
	install -m 0744 ${WORKDIR}/r41z_program.sh ${D}${bindir}

    install -d ${D}${sysconfdir}/init.d
    install -m 0755 ${WORKDIR}/load_zephyr ${D}${sysconfdir}/init.d
    update-rc.d -r ${D} load_zephyr start 70 5 .
}

FILES_${PN} += " \
	/usr/sbin/openocd \
	/usr/share/openocd/scripts/bitsbytes.tcl \
	/usr/share/openocd/scripts/memory.tcl \
	/usr/share/openocd/scripts/mmr_helpers.tcl \
	/usr/share/openocd/scripts/target/nrf52.cfg \
	/usr/share/openocd/scripts/target/kx.cfg \
	/usr/share/openocd/scripts/target/swj-dp.tcl \
	/usr/share/openocd/scripts/interface/imx-native.cfg \
	/usr/share/openocd/scripts/cpu/arm/arm7tdmi.tcl \
"

COMPATIBLE_MACHINE = "(vesta|imx6ul)"
