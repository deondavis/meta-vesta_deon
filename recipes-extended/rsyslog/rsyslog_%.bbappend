SUMMARY = "Add local.1 facility to rsyslog.conf for dgua logging"

FILESEXTRAPATHS_append := "${THISDIR}/${PN}:"

SRC_URI_append = " file://dgua.logrotate"


do_install_append() {
	sed -i '/user.log/ a local1.*                        -/tmp/swu_packages/dgua/dgua.log' ${D}${sysconfdir}/rsyslog.conf
	cat ${WORKDIR}/dgua.logrotate >> ${D}${sysconfdir}/logrotate.rsyslog
}
