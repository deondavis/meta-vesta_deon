# Copyright (C) 2016 Rigado 
SUMMARY = "IOT Gateway Setup Button"
DESCRIPTION = "Source and daemon for monitoring the user button"
LICENSE = "CLOSED"

SRC_URI = "file://iot-gateway-setup-button.c \
    file://gpio-utils.h \
    file://gpio-utils.c \
    file://iot-gateway-setup-buttond \
"

S = "${WORKDIR}"

do_compile() {
    ${CC} iot-gateway-setup-button.c gpio-utils.c -o iot-gateway-setup-button ${LDFLAGS} 
}

do_install() {
    install -d ${D}/usr/lib/node_modules/iot-gateway-setup/bin
    install -m 0755 iot-gateway-setup-button ${D}/usr/lib/node_modules/iot-gateway-setup/bin

    install -d ${D}${sysconfdir}/init.d
    install -m 0755 iot-gateway-setup-buttond ${D}${sysconfdir}/init.d
    update-rc.d -r ${D} iot-gateway-setup-buttond start 99 2 3 4 5 . stop 99 0 6 .
}

FILES_${PN} += " \
    /usr/lib/node_modules/iot-gateway-setup/bin/iot-gateway-setup-button \
"

COMPATIBLE_MACHINE = "(vesta|imx6ul)"

