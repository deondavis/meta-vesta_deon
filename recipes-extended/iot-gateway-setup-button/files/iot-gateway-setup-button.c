#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>
#include "gpio-utils.h"

#define USER_BUTTON 138
#define TIMEOUT -1 //forever 
#define SECONDS_TO_HOLD 8
/* This program will monitor the USER_BUTTON for a press of longer than SECONDS_TO_HOLD. */
/* It will then launch the configure_gateway application */

int main()
{
	struct pollfd fd_poll;
	int gpio_fd, rc;
	char buf[MAX_BUF];
	unsigned int gpio;
	int len;
	unsigned int value=0;
	int count = 0;

	gpio_export(USER_BUTTON);
	gpio_set_dir(USER_BUTTON, "in");
	gpio_set_edge(USER_BUTTON, "rising");  // Can be rising, falling or both
	gpio_fd = gpio_fd_open(USER_BUTTON, O_RDONLY);

	while (1) {
		fd_poll.fd = gpio_fd;
		fd_poll.events = POLLPRI;

		rc = poll(&fd_poll, 1, TIMEOUT);      

		if (rc < 0) {
			printf("\npoll() failed!\n");
			return -1;
		}
      
		if (fd_poll.revents & POLLPRI) {
			lseek(fd_poll.fd, 0, SEEK_SET);  // Read from the start of the file
			len = read(fd_poll.fd, buf, MAX_BUF);

			count = 0;
			gpio_get_value(USER_BUTTON, &value);

			while( value && count < SECONDS_TO_HOLD) 
			{
				sleep(1);
				count++;
				gpio_get_value(USER_BUTTON, &value);
			}	

			if(count >= SECONDS_TO_HOLD){
				if(system("/usr/lib/node_modules/iot-gateway-setup/bin/configure_gateway --enable") == -1)
					printf("error");	
			}
		}

		fflush(stdout);
	}

	gpio_fd_close(gpio_fd);
	return 0;
}

