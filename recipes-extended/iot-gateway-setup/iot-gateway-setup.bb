SUMMARY = "IOT Gateway Setup"
DESCRIPTION = "Installs the necessary packages to configure wpa_supplicant through a wireless access point."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=ea398a763463b76b18da15f013c0c531"

RDEPENDS_${PN} += " dnsmasq hostapd python iot-gateway-setup-button"

S = "${WORKDIR}/git"

SRCREV="9123254774c1fa5b73649ccbde5a3deaa2708bc7"
SRCBRANCH = "master"

SRC_URI = "git://git.rigado.com/vesta/iot-gateway-setup.git;protocol=https;branch=${SRCBRANCH} \
    file://iot-gateway-setup-server \
    file://hostapd.conf \
    file://dnsmasq.conf"

inherit npm-install-global

do_configure() {
    :
}

do_install_append() {
    install -d ${D}/var/lib/gateway_config_tools
    install -d ${D}${sysconfdir}/init.d
    install -m 755 ${WORKDIR}/iot-gateway-setup-server ${D}${sysconfdir}/init.d/iot-gateway-setup-server
    install -m 644 ${WORKDIR}/hostapd.conf ${D}${sysconfdir}/hostapd.conf
    install -m 644 ${WORKDIR}/dnsmasq.conf ${D}${sysconfdir}/dnsmasq.conf
}

COMPATIBLE_MACHINE = "(vesta|imx6ul)"

