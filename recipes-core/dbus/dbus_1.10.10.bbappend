SUMMARY = "D-Bus message bus"
DESCRIPTION = "run dbus init script after rng-tools and networking init"

INITSCRIPT_NAME = "dbus-1"
INITSCRIPT_PARAMS = "start 03 5 3 2 . stop 20 0 1 6 ."
