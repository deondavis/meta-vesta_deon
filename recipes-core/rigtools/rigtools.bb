# Copyright (C) 2016 Rigado

SUMMARY = "Rigado's general purpose tool"
DESCRIPTION = "A tool for the gateway which changes functionality based on how it is linked"

# The latest commit
SRCREV = "6b29a01024678cbbc3a9c43ae7b9393ace76341b"
SRCBRANCH ?= "v2.5.0"

require recipes-core/rigtools/rigtools.inc

