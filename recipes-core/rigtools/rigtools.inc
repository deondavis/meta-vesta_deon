SECTION = "base"
LICENSE = "CLOSED"

PROVIDES += "rigtool"
RDEPENDS_${PN} = "libcurl"
DEPENDS = "curl"
SRC_URI = "git://git.rigado.com/vesta/rigtools.git;protocol=https;branch=${SRCBRANCH}"
S = "${WORKDIR}/git"

do_configure() {

    if [ ${DEFAULTTUNE_mx6ul} = "cortexa7thf-neon" ]; then
        cp ${S}/rigtools-hf ${S}/rigtools
    else
        cp ${S}/rigtools-sf ${S}/rigtools
    fi

}


do_install() {
    install -d ${D}/etc
    install -m 755 ${S}/dgua.conf ${D}/etc/dgua.conf

    install -d ${D}/usr/bin
    install -m 755 ${S}/rigtools ${D}/usr/bin/rigtools

    install -d ${D}/etc/init.d
    install -d ${D}/etc/rc5.d
    install -m 755 ${S}/dgua_service ${D}/etc/init.d/dgua_service
    ln -sf /etc/init.d/dgua_service ${D}/etc/rc5.d/S70dgua_service
    
    ln -sf /usr/bin/rigtools ${D}/usr/bin/boot_counter
    ln -sf /usr/bin/rigtools ${D}/usr/bin/boot_toggle
    ln -sf /usr/bin/rigtools ${D}/usr/bin/dgua
    ln -sf /usr/bin/rigtools ${D}/usr/bin/dgua_service
    ln -sf /usr/bin/rigtools ${D}/usr/bin/board_serial_number
    ln -sf /usr/bin/rigtools ${D}/usr/bin/unit_serial_number
}

FILES_${PN} += " \
    /etc/dgua.conf \
    /etc/init.d/dgua_service \
    /usr/bin/rigtools \
    /usr/bin/boot_counter \
    /usr/bin/boot_toggle \
    /usr/bin/board_serial_number \
    /usr/bin/unit_serial_number \
    /usr/bin/dgua \
    /usr/bin/dgua_service \
"

