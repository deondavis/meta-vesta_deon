SUMMARY = "QCA6234 startup scripts"
DESCRIPTION = "Startup scripts to be used with the QCA6234"
SECTION = "base"
LICENSE = "CLOSED"

SRC_URI = "file://setmac.sh \
           file://hciattach.sh \
           file://abtfilt.sh \
"
S = "${WORKDIR}"


do_install() {
    install -d ${D}${sysconfdir}/init.d
    install -m 0755 setmac.sh ${D}${sysconfdir}/init.d
    update-rc.d -r ${D} setmac.sh start 60 S .
    
    install -m 0755 hciattach.sh ${D}${sysconfdir}/init.d
    update-rc.d -r ${D} hciattach.sh start 61 S .

    install -m 0755 abtfilt.sh ${D}${sysconfdir}/init.d
    update-rc.d -r ${D} abtfilt.sh start 60 2 3 4 5 . stop 40 0 6 .
}

COMPATIBLE_MACHINE = "(vesta|imx6ul)"
