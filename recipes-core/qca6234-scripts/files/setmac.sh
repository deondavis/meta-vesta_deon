#!/bin/sh

# If QCA6234 softmac.bin and/or ark3bdaddr.pst files don't exist, create them
# using MAC IDs that were burnt into eFuses during gateway manufacture.
# Then load ath6kl_sdio.ko module. 

get_fuse () {
    local name=$1
    local bytes=$2
    # read the fuse and pad the left with zeros to the size of $pad
    local fuse=$(cat /sys/fsl_otp/$name | cut -b 3-)
    echo ${fuse}
}

get_mac () {
    local upper_fuse=$1
    local upper_mac=$(get_fuse HW_OCOTP_GP$upper_fuse)
    local lower_mac=$(get_fuse HW_OCOTP_GP$(( $upper_fuse - 1 )))
    echo $upper_mac$lower_mac
}

MODVER=$(ls /lib/modules)

if [ ! -f /lib/firmware/ath6k/AR6004/hw3.0/softmac.bin ] ; then
    #QCA6234 WiFi softmac file not present, so create it 
	echo -ne $(echo $(get_mac 31) | sed -e 's/../\\x&/g') > /tmp/hold> /lib/firmware/ath6k/AR6004/hw3.0/softmac.bin
fi
if [ ! -f /lib/firmware/ar3k/1020201/ar3kbdaddr.pst ] ; then
    #QCA6234 Bluetooth ar3kbddar.pst file not present, so create it 
	echo $(get_mac 33) > /lib/firmware/ar3k/1020201/ar3kbdaddr.pst
fi
if [ -f /lib/firmware/ath10k/wlan/qcom_cfg.ini ] ; then
    #QCA9377 config file is present; make sure it has correct mac info
    sed -i "/Intf0MacAddress/ c\Intf0MacAddress=$(get_mac 31)" /lib/firmware/ath10k/wlan/qcom_cfg.ini
    sed -i '/Intf[1-3]MacAddress/d' /lib/firmware/ath10k/wlan/qcom_cfg.ini
    #And then try starting the driver.
#   insmod /lib/modules/$MODVER/extra/wlan.ko
fi

#install ath6kl driver module
insmod /lib/modules/$MODVER/kernel/drivers/net/wireless/ath/ath6kl/ath6kl_sdio.ko
#disable ath6kl driver runtime power management
iwconfig wlan0 power off
