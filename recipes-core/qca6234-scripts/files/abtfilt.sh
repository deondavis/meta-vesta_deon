#! /bin/sh
#
# Start bluettooh coexistance user-space daemon.  This has to occur after
# the dbus has been started, or the daemon will crash

case "$1" in
  start)
	abtfilt -d -a -x -w wlan0 -b -s
	;;
  stop)
	start-stop-daemon -K -q -n abtfilt
	;;
  *)
	echo "Usage: /etc/ini.d/abtfilt.sh {start|stop}"
	exit 1
esac

exit 0

