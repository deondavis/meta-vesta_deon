#!/bin/sh

url="rigadogateway.com"

get_fuse () {
    local name=$1
    local pad=$2
    # read the fuse and pad the left with zeros to the size of $pad
    local fuse="0000000000"$(cat /sys/fsl_otp/$name | cut -b 3-)
    len=$(( ${#fuse} - $pad ))
    echo ${fuse:$len}
}

get_sn_unit () {
    local arena=$(get_fuse $USN_FUSE1 7)
    if [[ ${arena:0:2} == "00" ]] ; then
        # starts with "00", so doesn't start with ascii hex value
        arena=${arena:2}
    else
        # doesn't start with "00", so starts with ascii hex value
        arena=$(printf "\x${arena:0:2}${arena:2}")
    fi

    local build=$(get_fuse $USN_FUSE2 4)
    local unit=$(get_fuse $USN_FUSE3 5)

    echo "$arena$build-$unit"
}

get_sn_board () {
    local arena=$(get_fuse $BSN_FUSE1 7)
    if [[ ${arena:0:2} == "00" ]] ; then
        # starts with "00", so doesn't start with ascii hex value
        arena=${arena:2}
    else
        # doesn't start with "00", so starts with ascii hex value
        arena=$(printf "\x${arena:0:2}${arena:2}")
    fi

    local build=$(get_fuse $BSN_FUSE2 4)
    local unit=$(get_fuse $BSN_FUSE3 5)

    echo "$arena$build-$unit"
}


#Read create custom serial number from fuses
get_custom_sn() {
    local fuse_70=`cat /sys/fsl_otp/$CSN_FUSE1`                 
    local fuse_71=`cat /sys/fsl_otp/$CSN_FUSE2`               
    if [ "$CSN_FUSE3" = "HW_OCOTP_GP72" ]; then
        local fuse_72=`cat /sys/fsl_otp/$CSN_FUSE3`  
    else
        local fuse_72=0x0
    fi
    local strhex

    #Check for  empty fuse bank
    if [ ${fuse_72} != "0x0" ]; then
       strhex=${fuse_72:2:8}
    fi

    #Check for empty fuse bank
    if [ ${fuse_71} != "0x0" ]; then
       strhex=${strhex}${fuse_71:2:8}
    fi

    strhex=${strhex}${fuse_70:2:8}

    local i=1
    max=$(( ${#strhex} + 1 ))

    #convert hex string to ascii string
    while [ $i -lt $max ]
    do
            hex='\x'`echo $strhex | cut -c $i-$(( i + 1 ))`
            custom_sn=$custom_sn$hex
            i=$(( i + 2 ))
    done

    echo -e $custom_sn
}

if [[ "$(cat /proc/cmdline | cut b -35-45)" = "vesta-300V2" ]] ; then
	CUST_FUSE1=HW_OCOTP_GP42
	CUST_FUSE2=HW_OCOTP_GP43
	CUST_FUSE2=NA
    USN_FUSE1=HW_OCOTP_GP33
    USN_FUSE2=HW_OCOTP_GP40
    USN_FUSE3=HW_OCOTP_GP41
    BSN_FUSE1=HW_OCOTP_GP30
    BSN_FUSE2=HW_OCOTP_GP31
    BSN_FUSE3=HW_OCOTP_GP32
else
	CUST_FUSE1=HW_OCOTP_GP70
	CUST_FUSE2=HW_OCOTP_GP71
	CUST_FUSE3=HW_OCOTP_GP72
    USN_FUSE1=HW_OCOTP_GP37
    USN_FUSE2=HW_OCOTP_GP38
    USN_FUSE3=HW_OCOTP_GP39
    BSN_FUSE1=HW_OCOTP_GP34
    BSN_FUSE2=HW_OCOTP_GP35
    BSN_FUSE3=HW_OCOTP_GP36
fi


if [[ "$(cat /sys/fsl_otp/$CUST_FUSE1)" != "0x0" ]] ; then
    hostname $(get_custom_sn)
elif [[ "$(cat /sys/fsl_otp/$USN_FUSE1)" == "0x0" ]] ; then
    # No unit serial number set, use board serial number
    hostname $(get_sn_board).$url
else
    # unit serial number set, use it
    hostname $(get_sn_unit).$url
fi

if ! grep -q -i "127.0.0.0" /etc/hosts; then
        NAME=$(hostname)                                                        
        SHORTNAME=$(hostname | cut -b -16)                                      
        NEWLINE="127.0.0.0	$NAME	$SHORTNAME"                             
        sed -i "/127.0.0.1/a ${NEWLINE}" /etc/hosts
fi          
