# Copyright (C) 2016 Rigado 
SUMMARY = "Vesta scripts"
DESCRIPTION = "Vesta scripts"
SECTION = "base"
LICENSE = "CLOSED"

PACKAGE_ARCH = "${MACHINE_ARCH}"

SRC_URI = "file://blacklist.conf \
        file://led_control.sh \
        file://load_nxp_demo.sh \
        file://pwm-init \
        file://set_hwrevision.sh \
        file://npm_install_packages.sh \
        file://rig_init.sh \
        file://watchdog.sh \
        file://start_hci.sh \
        file://attach_hci.sh \
	file://find_expansion_card.sh \
	file://vz_cell_init.sh \
"
S = "${WORKDIR}"

do_install() {
    install -d ${D}${exec_prefix}/rigado/scripts
    install -m 755 led_control.sh ${D}${exec_prefix}/rigado/scripts/led_control.sh
    install -m 755 load_nxp_demo.sh ${D}${exec_prefix}/rigado/scripts/load_nxp_demo.sh
    install -m 755 rig_init.sh ${D}${exec_prefix}/rigado/scripts/rig_init.sh
    install -m 755 set_hwrevision.sh ${D}${exec_prefix}/rigado/scripts/set_hwrevision.sh
    install -m 755 watchdog.sh ${D}${exec_prefix}/rigado/scripts/watchdog.sh
    install -m 755 npm_install_packages.sh ${D}${exec_prefix}/rigado/scripts/npm_install_packages.sh
    install -m 755 attach_hci.sh ${D}${exec_prefix}/rigado/scripts/attach_hci.sh

    install -d ${D}${sysconfdir}/rc5.d
    ln -sf /usr/rigado/scripts/rig_init.sh ${D}${sysconfdir}/rc5.d/S90rig_init.sh

    install -d ${D}${sysconfdir}/init.d

    install -m 0755 start_hci.sh ${D}${sysconfdir}/init.d

    install -m 0755 find_expansion_card.sh ${D}${sysconfdir}/init.d
    update-rc.d -r ${D} find_expansion_card.sh start 85 2 3 4 5 . stop 15 0 6 .

    install -m 0755 vz_cell_init.sh ${D}${sysconfdir}/init.d

    install -d ${D}${sysconfdir}/vesta

    install -d ${D}${sysconfdir}/modprobe.d
    install -m 0644 blacklist.conf ${D}${sysconfdir}/modprobe.d

    install -m 0755 pwm-init ${D}${sysconfdir}/init.d
    update-rc.d -r ${D} pwm-init start 10 2 3 4 5 .
}

FILES_${PN} += " \
    ${exec_prefix}/rigado/scripts/led_control.sh \
    ${exec_prefix}/rigado/scripts/load_nxp_demo.sh \
    ${exec_prefix}/rigado/scripts/rig_init.sh \
    ${exec_prefix}/rigado/scripts/set_hwrevision.sh \
    ${exec_prefix}/rigado/scripts/watchdog.sh \
    ${exec_prefix}/rigado/scripts/npm_install_packages.sh \
    ${exec_prefix}/rigado/scripts/attach_hci.sh \
"

COMPATIBLE_MACHINE = "(vesta|imx6ul)"
