#!/bin/sh

TYPE=$(board_serial_number | cut -b 3,4)

if [ "$TYPE" != "80" ] && [ "$TYPE" != "82" ]
then
	echo "The NXP demo is only supported on Vesta-200R or 300R units"
	exit 0
fi

echo ""
echo "This script will configure your Vesta Gateway to support the NXP"
echo "Thread demo.  To do this, it will load NXP's KW41Z Thread binary"
echo "onto the Gateway's R41Z unit, and reconfigure the Gateway's kernel"
echo "initialization scripts to enable communication with NXP's Volansys"
echo "Modular Gateway Manager Android application.  Due to resource"
echo "conflicts, this script will also disable the Gateway's wireless" 
echo "configuration capability."
echo " "
echo "Do you wish to proceed? (Y/N)"

read RESPONSE

case "$RESPONSE" in
	Y) 
		echo "loading NXP's thread firmware to R41Z . . . ."
		echo " "
		;;
	*) 
		echo  "exiting without making configuration changes"
		exit 0
		;;
	esac

r41z_program.sh -b /lib/firmware/r41z/nxp-thread-r41z.bin,0x00000000

echo " "
echo "Modifying Kernel init to start NXP demo processes during boot . . ."
echo " "
sleep 3

# don't start iot-gateway-setup-buttond daemon, as NXP demo needs to use 
# gateway's user button

update-rc.d -f iot-gateway-setup-buttond remove

# For NXP demo, hciattach.sh must be run from the same shell that starts the 
# wificommission process, so don't run hciattach.sh at boot time

update-rc.d -f hciattach.sh remove


# don't add respawn commands to inittab if this script is run more than once.

if [ ! -L "/etc/rc5.d/S99BRDeamons" ]
then
	echo "781:23456:respawn:/usr/bin/DeviceControl" >> /etc/inittab
	echo "782:23456:respawn:/usr/bin/CloudRegistration" >> /etc/inittab
fi

# start processes that support NXP Thread demo during kernel init

update-rc.d BRDeamons start 99 2 3 4 5 . stop 1 0 6 .

echo "The gateway must be rebooted to finish setting up support"
echo "for the NXP Thread demo.  Do you wish to reboot now? (Y/n)"

read RESPONSE2

case "$RESPONSE2" in
	Y) 
		echo "Rebooting now . . . ."
		sleep 2
		reboot
		;;
	*) 
		echo  "Please remember to reboot the Gateway before using"
		echo "NXP's Volansys Modular Gateway Manager Android app"
		exit 0
		;;
	esac
