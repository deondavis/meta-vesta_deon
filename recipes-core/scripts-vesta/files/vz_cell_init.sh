#!/bin/sh

#Verizon cell modem expansion card IO pin definitions
# --- v3 BOARD!! ---

#reserved pins (should be grounded)
rsvd_GPIO4_IO17=113
rsvd_GPIO2_IO9=41
rsvd_GPIO2_IO10=42
rsvd_GPIO2_IO11=43
rsvd_GPIO2_IO12=44
rsvd_GPIO2_IO13=45
rsvd_GPIO1_IO2=2
rsvd_GPIO5_IO5=133
rsvd_GPIO3_IO1=65
rsvd_GPIO3_IO4=68

#used pins
cell_wakeup=117 	#GPIO4_IO21, ESPI_CLK
vbus_enable=118 	#GPIO4_IO22, ESPI_CS_N
cell_pwrkey=30 		#GPIO1_IO30, ESPI_MOSI
cell_status_n=31 	#GPIO1_IO31, ESPI_MISO
cell_pwr_shdn=8 	#GPIO1_IO8, SPDIF_OUT
cell_disable=23 	#GPIO1_IO23, CAN2_RX
cell_reset=22 		#GPIO1_IO22, CAN2_TX

adc_lower_limit=2110

gpio_set()
{
    pin_num=$1
    pin_dir=$2 
    pin_val=$3
    num_args=$#
    
    test -e "/sys/class/gpio/gpio$pin_num"
    if [ $? -ne "0" ]; then
        echo $pin_num > /sys/class/gpio/export
    fi
    
    echo $pin_dir > /sys/class/gpio/gpio${pin_num}/direction
    
    if [ $num_args -eq 3 ]; then
        echo $pin_val > /sys/class/gpio/gpio${pin_num}/value
    fi
}

gpio_read()
{
    pin_num=$1

    pin_value=$(cat /sys/class/gpio/gpio${pin_num}/value)
}

check_modem_on()
{
    modem_on=0

    #Get raw value of adc 
    raw=$(cat /sys/bus/iio/devices/iio:device0/in_voltage1_raw)

    #Compare raw value against limits
    if [ "$raw" -lt "$adc_lower_limit" ]; then
        modem_on=0
    else
        modem_on=1
    fi
}

set_verizon_card_gpio()
{
    gpio_set $cell_wakeup out 0 #this must be high for the modem to boot, can start disabled
    gpio_set $vbus_enable out 0 #start disabled (no usb attach)
    gpio_set $cell_pwrkey out 0 #do not assert until ready for actual boot
    gpio_set $cell_status_n in #status input
    gpio_set $cell_pwr_shdn out 0 #regulator does not need to be shutdown by default
    gpio_set $cell_disable out 0 #do not disable (can be used for power saving/stand-by mode once booted)
    gpio_set $cell_reset out 0 #is not driven while powering on

    #Additional signals (reserved)
    gpio_set $rsvd_GPIO4_IO17 out 0 #keep low
    gpio_set $rsvd_GPIO2_IO9 out 0 #keep low
    gpio_set $rsvd_GPIO2_IO10 out 0 #keep low
    gpio_set $rsvd_GPIO2_IO11 out 0 #keep low
    gpio_set $rsvd_GPIO2_IO12 out 0 #keep low
    gpio_set $rsvd_GPIO2_IO13 out 0 #keep low
    gpio_set $rsvd_GPIO1_IO2 out 0 #keep low
    gpio_set $rsvd_GPIO5_IO5 out 0 #keep low
    gpio_set $rsvd_GPIO3_IO1 out 0 #keep low
    gpio_set $rsvd_GPIO3_IO4 out 0 #keep low
}

# Boot Process:
# 1) check modem power is enabled (cell_pwr_shdn == 0)
#    F) if no, set low and wait (1 second)
# 2) check if modem is ON (VDD_EXT > 1.8V on ADC1)
#    F) if no, power on modem (toggle cell_pwrkey high for 300ms), enable vbus, and once status_n goes low, wait (14) seconds
#    T) if yes, check for modem USB device
#        T) if yes, modem is on and running, so exit        
#        F) if no, check VBUS is enabled (vbus_enable == 1)
#            F) if yes, force a reset (toggle cell_reset high for 300ms)
#            T) if no, enable vbus and wait for (5) seconds
# 3) kick off USB enumeration

boot_vz_modem()
{
    gpio_set $cell_disable out 0 #make sure modem is not disabled
    gpio_set $cell_wakeup out 1 #make sure modem is ready for power on / awake

    #1)check cell modem power
    gpio_read $cell_pwr_shdn
    if [ $pin_value -eq 1 ]; then
        echo "\nEnabling power for modem\n"
    	gpio_set $cell_pwr_shdn out 0 #enable cell power
    	usleep 1000000
    fi

    #2)check if modem is ON (VDD_EXT > 1.8V)
    check_modem_on
    if [ $modem_on -eq 0 ]; then

	#modem is OFF, initiate modem boot
    	echo "\nTurning modem ON\n"
    	gpio_set $cell_pwrkey out 1
    	usleep 300000 #wait 300ms
    	gpio_set $cell_pwrkey out 0

	#enable VBUS
    	echo "\nEnabling VBUS for modem\n"
    	gpio_set $vbus_enable out 1

    #wait for status signal to indicate power off is complete
	readstatus=1
    	while [ $readstatus -eq 1 ]; do
            usleep 1000000
            echo "\nChecking cell modem Status...\n"
            gpio_read $cell_status_n
            readstatus=$pin_value
    	done

	echo "\nCell modem is powering up...\n"
    	usleep 14000000 #boot time is >13 seconds according to hw guide

    else #cell modem is already ON

	echo "\nModem is ON\n"

	#allow time for the main board to fully boot
	#maybe check how long the system has been up to decide if this is necessary
	usleep 1000000

	if [ -e /dev/ttyUSB2 ]; then
	    echo "\nCell modem attached to USB\n"
	    return 0 #USB device is attached, modem should work
	else

	    #check if VBUS is enabled
	    gpio_read $vbus_enable
	    if [ $pin_value -eq 0 ]; then
                echo "\nEnabling VBUS for cell modem\n"
                gpio_set $vbus_enable out 1
                usleep 50000
            else
            	#modem is on but not responding - reset the modem
            	echo "Cell modem is not responding; forcing a modem reset"
            	gpio_set $cell_reset out 1
            	usleep 300000 #wait 300ms
            	gpio_set $cell_reset out 0
            	usleep 14000000 # boot time is >13 seconds according to hw guide
            fi
    	fi
    fi

    #3)kick off USB enumeration
    echo "\nInitiating USB enumeration for cell modem\n"
    cat /dev/bus/usb/002/001
    usleep 6000000
    if [ -e /dev/ttyUSB2 ]; then
	echo "\nCell modem attached to USB\n"
	return 0 #USB device is attached, modem should work
    else
	echo "\nUSB enumeration failed!\n"
        return 1
    fi

    echo "\nCell modem power ON is complete\n"
    return 0

}

#Start yellow LED flash
/usr/rigado/scripts/led_control.sh yellow flash

#Init IO for cellular modem
set_verizon_card_gpio
	
#Power ON cellular modem
boot_vz_modem

#connect to verizon network
if [ $? = "0" ]; then
    echo "connect to VZ network"
    /usr/sbin/quectel-CM -s vzwinternet &
fi

