#! /bin/sh
### BEGIN INIT INFO
# Provides:          finds and initializes Vesta expansion cards
# Required-Start:    $local_fs
# Should-Start:
# Required-Stop:     $local_fs
# Should-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Initialize Vesta expansion card
### END INIT INFO

# The definition of actions: (From LSB 3.1.0)
# start         start the service
# stop          stop the service
# restart       stop and restart the service if the service is already running,
#               otherwise start the service

# PATH should only include /usr/* if it runs after the mountnfs.sh script
PATH=/sbin:/usr/sbin:/bin:/usr/bin


. /etc/init.d/functions || exit 1

#
# Function that starts the daemon/service
#
do_start() {

    if [ -f /sys/bus/i2c/devices/1-0050/eeprom ]; then

	cat /sys/bus/i2c/devices/1-0050/eeprom | hexdump -C > /tmp/eeprom
	RESULT1=$(awk -v n=4 'NR==n' /tmp/eeprom | cut -b 72-77)
	RESULT2=$(awk -v n=5 'NR==n' /tmp/eeprom | cut -b 62-64)
	BOARD=$(echo $RESULT1$RESULT2)
	echo $BOARD

	if [ $BOARD = "310-00110" ]; then
	
	    echo "found verizon cell modem expansion card"
	    /etc/init.d/vz_cell_init.sh
        fi

	#add tests for additional expansion cards here as they become available

	rm /tmp/eeprom
    fi
}

#
# Function that stops the daemon/service
#
do_stop() {

    if [ -f /sys/bus/i2c/devices/1-0050/eeprom ]; then

	cat /sys/bus/i2c/devices/1-0050/eeprom | hexdump -C > /tmp/eeprom
	RESULT1=$(awk -v n=4 'NR==n' /tmp/eeprom | cut -b 72-77)
	RESULT2=$(awk -v n=5 'NR==n' /tmp/eeprom | cut -b 62-64)
	BOARD=$(echo $RESULT1$RESULT2)
	echo $BOARD

	if [ $BOARD = "310-00110" ]; then
	
	    killall quectel-CM
        fi

	#add tests for additional expansion cards here as they become available

	rm /tmp/eeprom

    fi
}

case "$1" in
start)
    do_start
    ;;
stop)
    do_stop || exit $?
    ;;
restart)                                                                       
    # Always start the service regardless the status of do_stop                
    do_stop                                                                    
    do_start                                                                   
    ;;       
*)
    echo "Usage: $0 {start|stop|restart}" >&2
    exit 3
    ;;
esac
