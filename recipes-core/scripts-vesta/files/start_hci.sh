#!/bin/sh
### BEGIN INIT INFO
# Provides: hciattach for bmd300 
# Required-Start:
# Required-Stop:
# Default-Start:     S
# Default-Stop:
### END INIT INFO

/usr/rigado/scripts/attach_hci.sh 2>&1 | logger -p user.info
