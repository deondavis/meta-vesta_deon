#!/bin/sh

reset_bmd()
{
    reset_pin=$1

    if [ ! -f /sys/class/gpio/gpio${reset_pin}/direction ];
    then
        #setup gpio pin
        echo $reset_pin > /sys/class/gpio/export
        echo out > /sys/class/gpio/gpio${reset_pin}/direction
    fi

    #hold 0.1s
    echo 0 > /sys/class/gpio/gpio${reset_pin}/value
    usleep 100000
    echo 1 > /sys/class/gpio/gpio${reset_pin}/value
}

generate_mac()
{
    #hash the device serial
    hash=`unit_serial_number | md5sum | grep -oE "[0-9a-f]+"`

    #truncate to lower 40 bits
    mac=`echo $hash | grep -oE "[0-9a-f]{11}$"`

    #add a leading 0xf to set the high bit for random address
    mac=f$mac

    #upper case
    mac=${mac^^}

    #add colons
    mac=`echo $mac | sed 's/.\{2\}/&\:/g' | sed 's/.$//'`  
}

#If this is a vesta-100 board set idx=0 otherwise 1
cat /proc/cmdline | grep "vesta-100"
idx=$?

#genmac
echo "generating mac address..."
generate_mac

#hard reset the bmd
echo "resetting bmd..."
reset_bmd 120

echo "detaching..."
killall -9 hciattach
sleep 2

#Skip if vesta-100 board
if [ $idx -ne 0 ]; then
    #attach wlan
    echo "attaching hci0 (wlan)..."
    hciattach -t 20 /dev/ttymxc0 ath3k
fi

#attach
echo "attaching hci$idx (bmd)..."
hciattach -t 20 -s 1000000 /dev/ttymxc2 any 1000000 flow

#verify hciattach was successful
if [ $? -eq 0 ]; then
    sleep 2

    echo "setting mac address: $mac"
    btmgmt --index $idx static-addr $mac
    sleep 2
    echo "auto-power enable"
    btmgmt --index $idx auto-power
    sleep 2
else
    echo "FAILED: attaching hci$idx (bmd)..."
    exit 1
fi

#scan when done?
#this actually will cause hci to detach since the process running hciattach is killed when you press ctrl+C to stop scanning
#so you should start the scan yourself

#if [ $# -eq 1 ] && [ $1 == scan ]; then
#    echo "start lescan..."
#    btmgmt --index $idx find -l &
#fi

exit 0
