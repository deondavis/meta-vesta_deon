#!/bin/sh
#enable pmic voltage boost for usb
i2cset -y 0 0x08 0x66 0x48

#Set hardware revision for SWUpdate if it
#does not exist
/usr/rigado/scripts/set_hwrevision.sh

#Initialize USB host
cat /dev/bus/usb/001/001

#Led control
/usr/rigado/scripts/led_control.sh green 
