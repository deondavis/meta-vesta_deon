#!/bin/sh                                                
                                                         
#get version of the vesta board                          
version=$(cat /proc/device-tree/model | awk '{print $6}')
                                         
#define pwm to use based on board version
if [ "$version" == "v1" ]; then
	PWMCHIP=pwmchip2                               
else
	PWMCHIP=pwmchip1                               
fi                        
                          
#duty cycle in nanoseconds
FLASH=500000000       
SOLID=0               
OFF=1000000000        
                                                
#set state to flashing                          
state=$SOLID                                    
                                                
greenled=/sys/class/pwm/$PWMCHIP/pwm0/duty_cycle
redled=/sys/class/pwm/pwmchip5/pwm0/duty_cycle

if [ $# == 2 ]; then
	if [ "flash" == "$2" ]; then
		state=$FLASH
	fi
fi

if [ "$1" == "green" ]; then
	echo $OFF > $redled
	sleep 1 
	echo $state > $greenled
fi 

if [ "$1" == "red" ]; then
	echo $OFF > $greenled
	sleep 1
	echo $state > $redled
fi

if [ "$1" == "yellow" ]; then
	echo $state > $greenled
	echo $state > $redled
fi

if [ "$1" == "off" ]; then
	echo $OFF > $greenled
	echo $OFF > $redled
fi


