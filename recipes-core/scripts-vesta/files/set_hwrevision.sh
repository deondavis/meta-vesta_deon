#!/bin/sh
# This script will create a hwrevision file for use
# by swupdate. This file can be removed once 
# hw version can be accessed via fuses.
filename=/etc/hwrevision

create_hwrevision()
{
        version=$(cat /proc/device-tree/model | awk '{print $6}')

        #define pwm to use based on board version
        if [ "$version" == "v1" ]; then
                echo "vesta 1.0" > /etc/hwrevision
        else
                echo "vesta 2.0" > /etc/hwrevision
        fi
}

if [ ! -f $filename ]; then
        echo $filename does not exist
        echo Creating /etc/hwrevision file
        create_hwrevision
fi