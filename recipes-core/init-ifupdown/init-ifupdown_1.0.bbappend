SUMMARY = "Basic TCP/IP networking init scripts and configuration files"
DESCRIPTION = "Move networking start number to 02 so it runs after rng-tools"

INITSCRIPT_NAME = "networking"
INITSCRIPT_PARAMS = "start 02 2 3 4 5 . stop 80 0 6 1 ."

FILESEXTRAPATHS_append := "${THISDIR}/files:"