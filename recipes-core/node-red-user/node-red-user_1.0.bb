SUMMARY = "Recipe for adding node-red user and group to an image"
DESCRIPTION = "If an image to be built include node-red package, \
               then the node-red user and group will be added"
HOMEPAGE = "http://nodered.org/"
SECTION = "configuration"
LICENSE = "MIT | Apache-2.0" 
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420 \
                    file://settings.js;beginline=1;endline=15;md5=16687d48a60fd0e3b31f39be34712c5d"
PR = "r1"

SRC_URI = "file://settings.js"

S = "${WORKDIR}"

NODERED_WORKDIR = "${localstatedir}/node-red"
NODERED_PORT = "8080"
NODERED_USER = "nobody"
NODERED_GROUP = "root"

do_install_append() {
    install -d -m 0755 ${D}${datadir}/node-red
    install -m 644 settings.js ${D}${datadir}/node-red
    sed -i 's,%WORKDIR%,${NODERED_WORKDIR}/,g' ${D}${datadir}/node-red/settings.js
    sed -i 's,%PORT%,${NODERED_PORT},g' ${D}${datadir}/node-red/settings.js
    
    install -d 0755 ${D}${NODERED_WORKDIR}
    chown -R ${NODERED_USER}:${NODERED_GROUP} ${D}${NODERED_WORKDIR}
}

FILES_${PN} += " \
    ${NODERED_WORKDIR}/ \
    ${datadir}/node-red/ \
"

CONFFILES_${PN} = " \
    ${datadir}/node-red/settings.js \
"
