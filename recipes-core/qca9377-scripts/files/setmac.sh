#!/bin/sh

# For vesta-500/cascade, there is only a single set of MAC address efuses,
# which specify the MAC address for the Ethernet port.  The Wifi device
# will read the same MAC address registers, but will add one to the result
# to determine the WiFi's MAC. Note that we never expect the lower MAC
# to be FFFFFFFF, so we don't have to worry about a carry into the upper
# MAC field.

get_fuse () {
    local name=$1
    local bytes=$2
    # read the fuse and pad the left with zeros to the size of $pad
    local fuse=$(cat /sys/fsl_otp/$name | cut -b 3-| tr [a-f] [A-F])
    echo ${fuse}
}

get_mac () {
    local upper_mac=$(get_fuse HW_OCOTP_MAC1)
    local lower_mac=$(echo "obase=16;ibase=16;$(get_fuse HW_OCOTP_MAC0)" | bc)
    lower_mac_inc=$(echo "obase=16;ibase=16;$lower_mac + 1" | bc)
    echo $upper_mac$lower_mac_inc
}

MODVER=$(ls /lib/modules)

#if wlan module is already installed, remove it
if [ ! -z "$(lsmod | grep wlan)" ]; then
	rmmod wlan
fi

if [ -f /lib/firmware/ath10k/wlan/qcom_cfg.ini ] ; then
    #QCA9377 config file is present; make sure it has correct mac info
    sed -i "/Intf0MacAddress/ c\Intf0MacAddress=$(get_mac)" /lib/firmware/ath10k/wlan/qcom_cfg.ini
    sed -i '/Intf[1-3]MacAddress/d' /lib/firmware/ath10k/wlan/qcom_cfg.ini
    #And then reload the wlan.ko module to restart the ath10K driver.
    insmod /lib/modules/$MODVER/extra/wlan.ko
fi

#disable ath10 driver runtime power management
#iwconfig wlan0 power off
