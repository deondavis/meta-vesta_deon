SUMMARY = "QCA9377 startup scripts"
DESCRIPTION = "Startup scripts to be used with the QCA9377"
SECTION = "base"
LICENSE = "CLOSED"

SRC_URI = "file://setmac.sh \
           file://hciattach.sh \
	   file://wlan.conf \
"
S = "${WORKDIR}"


do_install() {
    install -d ${D}${sysconfdir}/init.d
    install -m 0755 setmac.sh ${D}${sysconfdir}/init.d
    update-rc.d -r ${D} setmac.sh start 60 S .
    
    install -m 0755 hciattach.sh ${D}${sysconfdir}/init.d
    update-rc.d -r ${D} hciattach.sh start 61 S .

    install -d ${D}${base_libdir}/modprobe.d
    install -m 0755 wlan.conf ${D}${base_libdir}/modprobe.d

}

FILES_${PN} += " \
	${base_libdir}/modprobe.d/wlan.conf \
"

COMPATIBLE_MACHINE = "(vesta|imx6ull)"
