#!/bin/sh
### BEGIN INIT INFO
# Provides:          node-red
# Required-Start:    $local_fs $remote_fs $network
# Required-Stop:     $local_fs $remote_fs $network
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start or stop the node-red server
### END INIT INFO

NAME=node-red
DAEMON=node-red-pi
SETTINGS=/usr/share/node-red/settings.js
FLOW=/usr/share/node-red/dev-kit.json
DAEMON_ARGS="--max-old-space-size=128 -s ${SETTINGS} ${FLOW}"
LOG_DIR="/var/log/node-red"
LOG="${LOG_DIR}/node-red.log"
PID=/run/node-red.pid
USER=nobody
USER_GROUP=root

if [[ ! -e ${LOG_DIR} ]]; then
    mkdir -p ${LOG_DIR} 
fi

chown -R $USER:$USER_GROUP ${LOG_DIR}
# grant node binary cap_net_raw privileges, so it can
# start/stop BLE advertising
setcap cap_net_raw+eip $(eval readlink -f `which node`)

case "$1" in
        start)
            echo -n "Starting $NAME: "
            start-stop-daemon --start --quiet --background --make-pidfile --chuid $USER:$USER_GROUP \
                --pidfile $PID --name $NAME --startas /bin/bash -- -c "exec $DAEMON $DAEMON_ARGS > $LOG 2>&1"
            echo "$NAME."
            ;;
        stop)
            echo -n "Stopping $NAME: "
            start-stop-daemon --stop --quiet --name $NAME --user $USER --pidfile $PID \
                --retry 30 --signal INT
            rm -f $PID
            echo "$NAME."
            ;;
        restart)
            echo "Restarting $NAME..."
            $0 stop
            sleep 1 
            $0 start
            echo "Done."
		    ;;
        *)
            N=/etc/init.d/$NAME
            echo "Usage: $N {start|stop|restart}" >&2
            exit 1
            ;;
esac

exit 0

