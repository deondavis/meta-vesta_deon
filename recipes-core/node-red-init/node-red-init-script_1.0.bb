SUMMARY = "Add init script for Node-RED to an image"
SECTION = "configuration"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

PR = "r1"

SRC_URI = "file://node-red"

S = "${WORKDIR}"

RDEPENDS_${PN} += " node-red node-red-user bash"

inherit update-rc.d

INITSCRIPT_NAME = "node-red"

INITSCRIPT_PARAMS = "start 99 2 3 4 5 . stop 20 0 1 6 ."

do_install() {
    install -d ${D}${sysconfdir}/init.d
    install -p -m 755 node-red ${D}${sysconfdir}/init.d/
}

FILES_${PN} = "${sysconfdir}/init.d/node-red"
