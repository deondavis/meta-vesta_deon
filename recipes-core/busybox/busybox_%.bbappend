FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

PACKAGES_remove = "${PN}-syslog"
RRECOMMENDS_${PN}_remove = " ${PN}-syslog"

SRC_URI += " \
    file://no_syslog.cfg \
        "
