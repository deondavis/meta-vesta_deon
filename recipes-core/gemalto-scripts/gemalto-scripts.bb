SUMMARY = "Gemalto modem startup scripts"
DESCRIPTION = "Startup scripts to be used with the Gemalto Cellular modem card"
SECTION = "base"
LICENSE = "CLOSED"

SRC_URI = "file://att_lte_chat \
           file://att_lte_ttyACM0 \
"
S = "${WORKDIR}"


do_install() {
    install -d ${D}${sysconfdir}/chatscripts
    install -m 0755 att_lte_chat ${D}${sysconfdir}/chatscripts
    

    install -d ${D}${sysconfdir}/ppp/peers
    install -m 0755 att_lte_ttyACM0 ${D}${sysconfdir}/ppp/peers
}

COMPATIBLE_MACHINE = "(vesta|imx6ul)"
