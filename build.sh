su chewbacca
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
cd
cd vesta-gateway-bsp-master/
/home/chewbacca/bin/repo sync
sed -i '0,/EULA_ACCEPTED/s/EULA_ACCEPTED=/EULA_ACCEPTED=1/' setup-environment
MACHINE=vesta-300 DISTRO=poky source setup-environment build 
sed -i '/MACHINE/c\MACHINE ??= "vesta-300"' conf/local.conf
bitbake vesta-image-developer

if [ $? -ne 0 ]; then 
    echo vesta-300-developer build FAILED
    exit 1
fi